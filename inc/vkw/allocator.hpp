#pragma once

#include <vkw_export.h>
#include <vk_mem_alloc.h>
#include <vkw/image.hpp>
#include <vkw/buffer.hpp>

namespace vkw {

class Buffer;
class DepthBuffer;

class VKW_EXPORT Allocator {
public:
    Allocator();
    Allocator(VkPhysicalDevice, VkDevice);
    ~Allocator();
    
    Allocator(const Allocator&) = delete;
    Allocator(Allocator&&);

    Allocator& operator=(const Allocator&) = delete;
    Allocator& operator=(Allocator&&);

    VmaAllocator handle() const;

    Buffer alloc_cpu(VkDeviceSize, VkBufferUsageFlags) const;
    Buffer alloc_gpu(VkDeviceSize, VkBufferUsageFlags) const;

    Image alloc_img(const Image::Configuration&) const;

    /**
     * @brief Create a depth buffer object
     * 
     * @param extent 
     * @param queue_family_idx The index of the queue family used for submitting image layout transition commands
     * @param queue_idx The index of the queue used for submitting image layout transition commands
     * @return DepthBuffer 
     */
    DepthBuffer create_depth_buffer(VkExtent2D extent, uint32_t queue_family_idx, uint32_t queue_idx) const;

private:
    VmaAllocator handle_;
};

template <class T>
vkw::Buffer load_data(vkw::Allocator* allocator, const std::vector<T>& src, uint32_t queue_family, VkBufferUsageFlags flags) {
    VkDeviceSize size = sizeof(T) * src.size();
    vkw::Buffer staging = allocator->alloc_cpu(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
    staging.copy(src);
    return staging.transfer(queue_family, flags);
}

VkDevice get_allocator_device(VmaAllocator);

}
