#pragma once

#include <vkw_export.h>
#include <vk_mem_alloc.h>
#include <vkw/image.hpp>
#include <stdexcept>
#include <vector>
#include <cstring>

namespace vkw {

class VKW_EXPORT Buffer {
public:
    Buffer();
    Buffer(VmaAllocator, VkDeviceSize, VkBufferUsageFlags, VmaMemoryUsage);
    ~Buffer();

    Buffer(const Buffer&) = delete;
    Buffer(Buffer&&);

    Buffer& operator=(const Buffer&) = delete;
    Buffer& operator=(Buffer&&);

    VkBuffer handle() const;
    VkDeviceSize offset() const;

    template <class T>
    vkw::Buffer& copy_items(const std::vector<T>& data) {
        return load(reinterpret_cast<const void *>(data.data()), data.size() * sizeof(T));
    }

    template <class T>
    vkw::Buffer& copy(const T& data) {
        return load(reinterpret_cast<const void*>(&data), sizeof(T));
    }
    
    Buffer transfer(uint32_t submit_queue, VkBufferUsageFlags flags) const;

    Image create_image(uint32_t submit_queue, const Image::Factory& factory, VkImageLayout image_layout, VkPipelineStageFlags stage_flags, VkAccessFlags access_mask, VkImageAspectFlags flags) const;

    vkw::Buffer& load(const void* source, size_t size);

private:
    VmaAllocator allocator_;
    VmaAllocation allocation_;
    VkBuffer handle_;
    VkDeviceSize requested_size_;
};

}
