#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.hpp>
#include <vector>
#include <functional>

namespace vkw {

class VKW_EXPORT CommandBufferBegin {
public:
	CommandBufferBegin(VkCommandBuffer, VkCommandBufferUsageFlags);
	~CommandBufferBegin();

	CommandBufferBegin(const CommandBufferBegin&) = delete;
	CommandBufferBegin(CommandBufferBegin&&);

	CommandBufferBegin& operator=(const CommandBufferBegin&) = delete;
	CommandBufferBegin& operator=(CommandBufferBegin&&);

	VkCommandBuffer handle() const;

	VkResult end();

private:
	VkCommandBuffer handle_;
};

class VKW_EXPORT CommandPool {
public:
	class Allocation;

	CommandPool();
	CommandPool(VkDevice, uint32_t, VkAllocationCallbacks* = nullptr);
	CommandPool(VkDevice, uint32_t, VkCommandPoolCreateFlags, VkAllocationCallbacks* = nullptr);
	~CommandPool();

	CommandPool(const CommandPool&) = delete;
	CommandPool(CommandPool&&);

	CommandPool& operator=(const CommandPool&) = delete;
	CommandPool& operator=(CommandPool&&);

	Allocation allocate(size_t, VkCommandBufferLevel = VK_COMMAND_BUFFER_LEVEL_PRIMARY);

private:
	VkDevice device_;
	VkCommandPool handle_;
	VkAllocationCallbacks* alloc_callbacks_;
};

class VKW_EXPORT CommandPool::Allocation {
public:
	Allocation();
	Allocation(VkDevice, VkCommandPool, size_t, VkCommandBufferLevel);
	~Allocation();

	Allocation(const Allocation&) = delete;
	Allocation(Allocation&&);

	Allocation& operator=(const Allocation&) = delete;
	Allocation& operator=(Allocation&&);

	CommandBufferBegin begin(size_t, VkCommandBufferUsageFlags) const;

	VkCommandBuffer operator[](size_t) const;

	size_t size() const;

private:
	struct Pimpl;
	Pimpl *p;
};

}
