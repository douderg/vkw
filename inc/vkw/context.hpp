#pragma once

#include <vkw_export.h>
#include <vkw/instance.hpp>
#include <vkw/device.hpp>
#include <vkw/surface.hpp>
#include <vkw/allocator.hpp>
#include <vkw/validation.hpp>
#include <memory>

namespace vkw {

class VKW_EXPORT Context {
public:
    Context();
    ~Context();
#if defined(VKW_USE_SDL)
    Context(SDL_Window* window);
#endif

    Context(const Context&) = delete;
    Context& operator=(const Context&) = delete;

    Context(Context&& other);
    Context& operator=(Context&& other);

#if defined(VK_USE_PLATFORM_WIN32_KHR)
    void create_surface(HINSTANCE instance, HWND hwnd);
#endif
#if defined(VK_USE_PLATFORM_XLIB_KHR)
    void create_surface(Display* display, Window window);
#endif

    void create_device(VkPhysicalDevice device, Device::Configuration& config);
    
    VkInstance instance() const;

    Device& device();

    const Device& device() const;
    
    Allocator& allocator();

    const Allocator& allocator() const;

    Surface& surface();

    const Surface& surface() const;

    /**
     * @brief Get the index that is associated with the queue_family at the specified index.
     * 
     * @param position Position index in the internal list of queue family indices 
     * @return uint32_t The index that the device uses to refer to the target queue family
     */
    uint32_t get_queue_family_index(size_t position) const;

private:
    struct Pimpl;
    Pimpl *p;
};

}
