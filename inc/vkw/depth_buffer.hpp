#pragma once

#include <vkw_export.h>
#include <vk_mem_alloc.h>
#include <vkw/image.hpp>

namespace vkw {

class Image;
class ImageView;

class VKW_EXPORT DepthBuffer {
public:
    DepthBuffer() = default;
    DepthBuffer(VmaAllocator, VkExtent2D, uint32_t queue_family_index, uint32_t queue_index);
    ~DepthBuffer() = default;

    DepthBuffer(const DepthBuffer&) = delete;
    DepthBuffer(DepthBuffer&&);

    DepthBuffer& operator=(const DepthBuffer&) = delete;
    DepthBuffer& operator=(DepthBuffer&&);

    VkImage image() const;
    VkImageView view() const;

private:
    Image image_;
    ImageView view_;
};

}
