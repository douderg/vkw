#pragma once

#include "vkw_export.h"
#include <vulkan/vulkan.hpp>
#include <memory>

namespace vkw {


class VKW_EXPORT DescriptorSet {
public:
    class Layout;
private:
};


class VKW_EXPORT DescriptorPool {
public:

    class Allocation;

    class Factory;

    DescriptorPool();
    DescriptorPool(VkDevice device, uint32_t max_sets, const std::vector<VkDescriptorPoolSize>& sizes, VkAllocationCallbacks* callbacks = nullptr);
    ~DescriptorPool();

    DescriptorPool(const DescriptorPool&) = delete;
    DescriptorPool(DescriptorPool&&);

    DescriptorPool& operator=(const DescriptorPool&) = delete;
    DescriptorPool& operator=(DescriptorPool&&);

    VkDescriptorPool handle() const;

    Allocation alloc(VkDescriptorSetLayout) const;
    Allocation alloc(const std::vector<VkDescriptorSetLayout>&) const;

    VkDevice device() const;

private:
    VkDevice device_;
    VkDescriptorPool handle_;
    VkAllocationCallbacks* alloc_callbacks_;

};


class VKW_EXPORT DescriptorPool::Factory {
public:
    Factory();
    ~Factory();
    Factory(const Factory&);
    Factory& operator=(const Factory& other);
    Factory(Factory&&);
    Factory& operator=(Factory&&);

    Factory& add(uint32_t count, VkDescriptorType type);
    Factory& max_sets(uint32_t);
    DescriptorPool create(VkDevice device, VkAllocationCallbacks *callbacks = nullptr) const;
private:
    struct Pimpl;
    Pimpl* p;
};


class VKW_EXPORT DescriptorPool::Allocation {
public:
    class Update;

    Allocation();
    Allocation(VkDevice, VkDescriptorPool, const std::vector<VkDescriptorSetLayout>&);
    ~Allocation();
    Allocation(const Allocation& other) = delete;
    Allocation& operator=(const Allocation& other) = delete;
    Allocation(Allocation&& other);
    Allocation& operator=(Allocation&& other);

    Update write_set(size_t set_index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorBufferInfo>&, uint32_t start = 0) const;
    Update write_set(size_t set_index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorImageInfo>&, uint32_t start = 0) const;
    VkDescriptorSet operator[](size_t) const;
private:
    struct Pimpl;
    Pimpl* p;
};


/**
 * @brief RAII guard used to accumulate writes to descriptor sets which are performed as soon as the guard is destroyed.
 */
class VKW_EXPORT DescriptorPool::Allocation::Update {
public:
    Update(VkDevice device, std::shared_ptr<std::vector<VkDescriptorSet>> sets);
    ~Update();

    Update(const Update& other) = delete;
    Update(Update&& other);
    Update& operator=(const Update& other) = delete;
    Update& operator=(Update&& other);

    Update& write_set(size_t set_index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorBufferInfo>&, uint32_t start = 0);
    Update& write_set(size_t set_index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorImageInfo>&, uint32_t start = 0);
private:
    struct Pimpl;
    Pimpl*p;
};


class VKW_EXPORT DescriptorSet::Layout {
public:
    class Binding;
    class Factory;

    Layout();
    Layout(VkDevice device, VkDescriptorSetLayoutCreateInfo* info, VkAllocationCallbacks* alloc_callbacks = nullptr);
    Layout(const Layout& other) = delete;
    Layout(Layout&& other);
    ~Layout();

    Layout& operator=(const Layout& other) = delete;
    Layout& operator=(Layout&& other);

    VkDescriptorSetLayout handle() const;

private:
    VkDevice device_;
    VkDescriptorSetLayout handle_;
    VkAllocationCallbacks* alloc_callbacks_;
};


class VKW_EXPORT DescriptorSet::Layout::Factory {
public:
    Factory();
    ~Factory();
    Factory(const Factory& other);
    Factory& operator=(const Factory& other);

    Factory(Factory&& other);
    Factory& operator=(Factory&& other);

    Factory& add_binding(const Binding& binding);
    Factory& add_binding(uint32_t binding, VkDescriptorType type, VkShaderStageFlags stages, uint32_t count = 1);
    Layout create(VkDevice device, VkAllocationCallbacks* alloc_callbacks = nullptr);
private:
    struct Pimpl;
    Pimpl* p;
};


class VKW_EXPORT DescriptorSet::Layout::Binding {
public:
    Binding();
    Binding(uint32_t);
    ~Binding();
    
    Binding(const Binding& other);
    Binding& operator=(const Binding& other);

    Binding(Binding&& other);
    Binding& operator=(Binding&& other);

    Binding& binding(uint32_t);
    Binding& descriptors(uint32_t count, VkDescriptorType);
    Binding& stage_flags(VkShaderStageFlags);
    VkDescriptorSetLayoutBinding handle() const;
private:
    struct Pimpl;
    Pimpl* p;
};

}
