#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.hpp>
#include <vector>
#include <map>
#include <functional>
#include <optional>

namespace vkw {

VKW_EXPORT std::vector<VkPhysicalDevice> get_physical_devices(VkInstance);
VKW_EXPORT std::vector<VkQueueFamilyProperties> queue_family_properties(VkPhysicalDevice);
VKW_EXPORT std::vector<VkExtensionProperties> get_available_extensions(VkPhysicalDevice);
VKW_EXPORT std::vector<VkSurfaceFormatKHR> get_surface_formats(VkPhysicalDevice, VkSurfaceKHR);
VKW_EXPORT std::vector<VkPresentModeKHR> get_present_modes(VkPhysicalDevice, VkSurfaceKHR);
VKW_EXPORT VkSurfaceCapabilitiesKHR get_surface_capabilities(VkPhysicalDevice, VkSurfaceKHR);

// class VKW_EXPORT DeviceSelector {
// public:
// 	virtual ~DeviceSelector();
// 	virtual VkPhysicalDevice selection() const = 0;
// 	virtual std::vector<VkDeviceQueueCreateInfo> queue_infos() const = 0;
// 	virtual std::vector<const char*> required_extensions() const = 0;
// 	virtual std::vector<const char*> enabled_layers() const = 0;
// 	virtual std::vector<VkPhysicalDeviceFeatures> enabled_features() const = 0;
// 	virtual std::vector<uint32_t> queue_family_indexes() const = 0;
// 	virtual void reset() = 0;
// 	virtual bool accept(VkPhysicalDevice) = 0;
// };

class VKW_EXPORT Device {
public:
	class Configuration;

	Device();
	Device(VkInstance, const Configuration&, VkAllocationCallbacks* = nullptr);
	~Device();
	Device(Device&&);
	Device(const Device&) = delete;
	Device& operator=(Device&&);
	Device& operator=(const Device&) = delete;
	VkDevice handle() const;
	VkPhysicalDevice physical_device() const;
	// Device& select(DeviceSelector&);
private:
	VkInstance instance_;
	VkPhysicalDevice device_;
	VkDevice handle_;
	VkAllocationCallbacks* alloc_callbacks_;
};


class VKW_EXPORT Device::Configuration {
public:
	Configuration();
	~Configuration();

	Configuration(const Configuration& other) = delete;
	Configuration& operator=(const Configuration& other) = delete;

	VkPhysicalDevice device() const;
	const std::vector<VkDeviceQueueCreateInfo>& queue_infos() const;
	const std::vector<const char*>& required_extensions() const;
	const std::vector<const char*>& enabled_layers() const;
	const std::vector<VkPhysicalDeviceFeatures>& enabled_features() const;

	Configuration& required_extensions(const std::vector<const char*>& extensions);
	Configuration& enabled_layers(const std::vector<const char*>& layer_names);
	Configuration& enabled_features(const std::vector<VkPhysicalDeviceFeatures>& features);
	Configuration& prefer_if(std::function<bool(VkPhysicalDevice, VkPhysicalDevice)> compare);

	Configuration& add_queues(const std::vector<float>& priorities, VkSurfaceKHR surface, VkQueueFlags flags);
	Configuration& add_queues(const std::vector<float>& priorities, VkQueueFlags flags);
	Configuration& add_queues(const std::vector<float>& priorities, VkSurfaceKHR surface);

	Device create(VkInstance instance, VkPhysicalDevice hint = VK_NULL_HANDLE);

	std::vector<uint32_t> queue_family_indices() const;

private:
	struct Pimpl;
	Pimpl *p;

	bool is_appropriate(VkPhysicalDevice device) const;
	// std::optional<size_t> pick_queue_family_index(VkPhysicalDevice device, size_t queue_cfg_index) const;
};

}
