#ifndef _VKW_EVENT_HANDLER_HPP_
#define _VKW_EVENT_HANDLER_HPP_

#include "vkw_export.h"
#include <cinttypes>
#include <chrono>

namespace vkw {

enum class VKW_EXPORT Key : int {
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    num_0, num_1, num_2, num_3, num_4, num_5, num_6, num_7, num_8, num_9,
    arrow_left, arrow_right, arrow_up, arrow_down, space
};

enum class VKW_EXPORT MouseEvent : int {
    left_press, left_release, 
    right_press, right_release, 
    middle_press, middle_release, 
    wheel_scroll_up, wheel_scroll_down
};

class VKW_EXPORT EventHandler {
public:
    EventHandler() = default;
	virtual ~EventHandler() = default;
	virtual void on_key_press(Key, int, int);
	virtual void on_key_release(Key, int, int);
	virtual void on_mouse_button(MouseEvent, int, int);
	virtual void on_mouse_move(int, int);
	virtual void on_resize(uint32_t, uint32_t);
    virtual void on_time_passed(const std::chrono::steady_clock::duration&);
};

}

#endif