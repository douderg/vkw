#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.h>
#include <vector>

namespace vkw {

class VKW_EXPORT Framebuffer {
public:
    Framebuffer();
    Framebuffer(VkDevice, const std::vector<VkImageView>&, VkExtent2D, VkRenderPass, size_t, VkAllocationCallbacks* = nullptr);
    ~Framebuffer();

    Framebuffer(const Framebuffer&) = delete;
    Framebuffer(Framebuffer&&);

    Framebuffer& operator=(const Framebuffer&) = delete;
    Framebuffer& operator=(Framebuffer&&);

    VkFramebuffer handle() const;

private:
    VkDevice device_;
    VkFramebuffer handle_;
    VkAllocationCallbacks* alloc_callbacks_;
};

}
