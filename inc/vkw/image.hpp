#pragma once

#include <vkw_export.h>
#include <vk_mem_alloc.h>
#include <vkw/image_view.hpp>
#include <vkw/queues.hpp>
#include <vector>

namespace vkw {

class VKW_EXPORT Image {
public:
    class Configuration;
    class Subresource;
    class Factory;

    Image();
    Image(const Configuration&, VmaAllocator, VmaMemoryUsage);
    Image(const Configuration&, VmaAllocator, VmaMemoryUsage, const std::vector<uint32_t>&);
    ~Image();

    Image(const Image&) = delete;
    Image(Image&&);

    Image& operator=(const Image&) = delete;
    Image& operator=(Image&&);

    VkImage handle() const;

    ImageView create_view(VkImageAspectFlags) const;

    Image& change_layout(VkCommandBuffer, VkImageLayout, VkPipelineStageFlags, VkAccessFlags, VkImageAspectFlags);

private:
    VmaAllocator allocator_;
    VmaAllocation allocation_;
    VkImage handle_;
    VkFormat format_;
    uint32_t mip_levels_;
    uint32_t layers_;

    VkImageLayout layout_;
    VkPipelineStageFlags pipeline_stage_;
    VkAccessFlags access_flags_;
};


class VKW_EXPORT Image::Configuration {
public:
    virtual ~Configuration() = default;
    virtual VkImageType type() const = 0;
    virtual VkFormat format() const = 0;
    virtual VkExtent3D extent() const = 0;
    virtual uint32_t mip_levels() const = 0;
    virtual uint32_t layers() const = 0;
    virtual VkSampleCountFlagBits samples() const = 0;
    virtual VkImageTiling tiling() const = 0;
    virtual VkImageUsageFlags usage() const = 0;
};


class VKW_EXPORT Image::Factory : public Image::Configuration {
public:
    Factory();
    VkImageType type() const override;
    VkFormat format() const override;
    VkExtent3D extent() const override;
    uint32_t mip_levels() const override;
    uint32_t layers() const override;
    VkSampleCountFlagBits samples() const override;
    VkImageTiling tiling() const override;
    VkImageUsageFlags usage() const override;

    Factory& extent(uint32_t width, uint32_t height, uint32_t depth);
    Factory& extent(const VkExtent3D& extent);
    Factory& format(VkFormat fmt);
    Factory& usage(VkImageUsageFlags flags);

    Image create(VmaAllocator allocator, VmaMemoryUsage mem_usage) const;
private:
    VkImageType type_;
    VkFormat format_;
    VkExtent3D extent_;
    uint32_t levels_, layers_;
    VkSampleCountFlagBits samples_;
    VkImageTiling tiling_;
    VkImageUsageFlags usage_;

};

}
