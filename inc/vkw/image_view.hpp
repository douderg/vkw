#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.h>

namespace vkw {

class VKW_EXPORT ImageView {
public:
    ImageView();
    ImageView(VkDevice, VkImage, VkFormat, VkAllocationCallbacks* = nullptr);
    ImageView(VkDevice, VkImage, VkImageViewType, VkFormat, VkImageAspectFlags, VkAllocationCallbacks* = nullptr);
    ~ImageView();

    ImageView(const ImageView&) = delete;
    ImageView(ImageView&&);

    ImageView& operator=(const ImageView&) = delete;
    ImageView& operator=(ImageView&&);

    VkImageView handle() const;

private:
    VkDevice device_;
    VkImageView handle_;
    VkAllocationCallbacks* alloc_callbacks_;
};

}
