#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.hpp>

namespace vkw {

class VKW_EXPORT Instance {
public:
	class Configuration;

	Instance(VkAllocationCallbacks* = nullptr);
	Instance(const Configuration&, VkAllocationCallbacks* = nullptr);
	Instance(const Instance&) = delete;
	Instance(Instance&&);
	~Instance();

	Instance& operator=(const Instance&) = delete;
	Instance& operator=(Instance&&);

	VkInstance handle() const;

private:
	VkInstance handle_;
	const VkAllocationCallbacks* alloc_callbacks_;
};

class Instance::Configuration {
public:
	const std::vector<const char*>& extensions() const;
	const std::vector<const char*>& layers() const;
	Configuration& extensions(const std::vector<const char*>&);
	Configuration& layers(const std::vector<const char*>&);
private:
	std::vector<const char*> extensions_, layers_;
};

}
