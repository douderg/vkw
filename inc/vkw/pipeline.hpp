#pragma once

#include "vkw_export.h"
#include <vulkan/vulkan.hpp>
#include <unordered_map>

namespace vkw {

class VKW_EXPORT Viewport {
public:
	Viewport(float, float);
	Viewport(VkExtent2D);
	Viewport& offset(VkOffset2D);
	Viewport& offset(float, float);
	Viewport& extent(VkExtent2D);
	Viewport& extent(float, float);
	Viewport& depth(float, float);
	VkViewport handle() const;
private:
	VkViewport value_;
};

class VKW_EXPORT Scissor {
public:
	Scissor(VkExtent2D);
	Scissor(VkOffset2D, VkExtent2D);
	VkRect2D handle() const;
private:
	VkRect2D value_;
};

class VKW_EXPORT Pipeline {
public:
	class Layout;
	class VertexInputState;
	class InputAssemblyState;
	class ViewportState;
	class RasterizationState;
	class MultisampleState;
	class DepthStencilState;
	class ColorBlendState;
	class DynamicState;

	class Configuration;

	Pipeline();
	Pipeline(VkPipeline, VkDevice, VkAllocationCallbacks*);
	Pipeline(const Pipeline&) = delete;
	Pipeline(Pipeline&&);
	~Pipeline();
	Pipeline& operator=(const Pipeline&) = delete;
	Pipeline& operator=(Pipeline&&);
	VkPipeline handle() const;

private:
	VkDevice device_;
	VkPipeline handle_;
	VkAllocationCallbacks* alloc_callbacks_;
};

class VKW_EXPORT Pipeline::Layout {
public:
	class Configuration;

	Layout();
	Layout(VkDevice, const Configuration&, VkAllocationCallbacks* = nullptr);
	Layout(const Layout&) = delete;
	Layout(Layout&&);
	~Layout();

	Layout& operator=(const Layout&) = delete;
	Layout& operator=(Layout&&);
	VkPipelineLayout handle() const;

private:
	VkPipelineLayout handle_;
	VkDevice device_;
	VkAllocationCallbacks* alloc_callbacks_;
};

class Pipeline::Layout::Configuration {
public:
	VKW_EXPORT Configuration& add(VkDescriptorSetLayout);
	VkPipelineLayoutCreateInfo create_info() const;
private:
	std::vector<VkDescriptorSetLayout> set_layouts_;
	std::vector<VkPushConstantRange> push_constant_ranges_;
};


class VKW_EXPORT Pipeline::VertexInputState {
public:
	VertexInputState();
	~VertexInputState();

	VertexInputState(const VertexInputState&);
	VertexInputState(VertexInputState&&);

	VertexInputState& operator=(const VertexInputState&);
	VertexInputState& operator=(VertexInputState&&);

	VertexInputState &create_binding(uint32_t binding, VkVertexInputRate rate, uint32_t stride);
	VertexInputState &bind_attribute(uint32_t binding, uint32_t location, const VkFormat &fmt, uint32_t offset);
	const VkPipelineVertexInputStateCreateInfo* info() const;

private:
	struct Pimpl;
	Pimpl* p;
};


class VKW_EXPORT Pipeline::InputAssemblyState {
public:
	InputAssemblyState();
	InputAssemblyState(const InputAssemblyState&) = default;
	InputAssemblyState(InputAssemblyState&&) = default;

	InputAssemblyState& operator=(const InputAssemblyState&) = default;
	InputAssemblyState& operator=(InputAssemblyState&&) = default;

	InputAssemblyState& topology(VkPrimitiveTopology value);
	InputAssemblyState& primitive_restart(VkBool32 value);

	const VkPipelineInputAssemblyStateCreateInfo* info() const;

private:
	VkPipelineInputAssemblyStateCreateInfo info_;
};


class VKW_EXPORT Pipeline::ViewportState {
public:
	ViewportState();
	~ViewportState();

	ViewportState(const ViewportState&);
	ViewportState(ViewportState&&);

	ViewportState& operator=(const ViewportState&);
	ViewportState& operator=(ViewportState&&);

	ViewportState& add(const Viewport&);
	ViewportState& add(const Scissor&);
	const VkPipelineViewportStateCreateInfo* info() const;

private:
	struct Pimpl;
	Pimpl* p;
};


class VKW_EXPORT Pipeline::RasterizationState {
public:
	RasterizationState();
	RasterizationState(const RasterizationState&) = default;
	RasterizationState(RasterizationState&&) = default;

	RasterizationState& operator=(const RasterizationState&) = default;
	RasterizationState& operator=(RasterizationState&&) = default;

	RasterizationState& polygon_mode(VkPolygonMode value);
	RasterizationState& front_face(VkFrontFace value);
	RasterizationState& cull_mode(VkCullModeFlags value);
	RasterizationState& depth_bias_enabled(VkBool32 value);
	RasterizationState& depth_bias_constant_factor(float value);
	RasterizationState& depth_bias_clamp(float value);
	RasterizationState& depth_bias_slope_factor(float value);
	RasterizationState& line_width(float value);

	const VkPipelineRasterizationStateCreateInfo* info() const;

private:
	VkPipelineRasterizationStateCreateInfo info_;
};


class VKW_EXPORT Pipeline::MultisampleState {
public:
	MultisampleState();
	~MultisampleState();

	MultisampleState(const MultisampleState&);
	MultisampleState(MultisampleState&&);

	MultisampleState& operator=(const MultisampleState&);
	MultisampleState& operator=(MultisampleState&&);

	MultisampleState& samples_per_pixel(VkSampleCountFlagBits value);
	MultisampleState& sample_shading_enable(VkBool32 value);
	MultisampleState& min_sample_shading(float value);
	MultisampleState& sample_masks(const std::vector<VkSampleMask>& value);
	MultisampleState& alpha_to_coverage_enable(VkBool32 value);
	MultisampleState& alpha_to_one_enable(VkBool32 value);

	const VkPipelineMultisampleStateCreateInfo* info() const;

private:
	struct Pimpl;
	Pimpl* p;
	// VkPipelineMultisampleStateCreateInfo info_;
	// std::vector<VkSampleMask> sample_masks_;
};


class VKW_EXPORT Pipeline::DepthStencilState {
public:
	class StencilOpState {
	public:
		StencilOpState(DepthStencilState& state);
		StencilOpState& on_fail(VkStencilOp value);
		StencilOpState& on_pass(VkStencilOp value);
		StencilOpState& on_depth_fail(VkStencilOp value);
		StencilOpState& compare_op(VkCompareOp value);
		StencilOpState& compare_mask(uint32_t value);
		StencilOpState& write_mask(uint32_t value);
		StencilOpState& reference_mask(uint32_t value);
		DepthStencilState& front_test();
		DepthStencilState& back_test();
	private:
		DepthStencilState& state_;
		VkStencilOpState op_state_;
	};

	DepthStencilState();
	DepthStencilState(const DepthStencilState&) = default;
	DepthStencilState(DepthStencilState&&) = default;

	DepthStencilState& operator=(const DepthStencilState&) = default;
	DepthStencilState& operator=(DepthStencilState&&) = default;

	DepthStencilState& depth_test(VkBool32 value, VkCompareOp comp);
	DepthStencilState& depth_write(VkBool32 value);

	DepthStencilState& stencil_test(VkBool32 value);
	StencilOpState stencil_op();

	DepthStencilState& depth_bounds_test(VkBool32 value);
	DepthStencilState& depth_bounds(float min, float max);

	const VkPipelineDepthStencilStateCreateInfo* info() const;

private:
	VkPipelineDepthStencilStateCreateInfo info_;
};


class VKW_EXPORT Pipeline::ColorBlendState {
public:
	class VKW_EXPORT Attachment {
	public:
		Attachment();
		Attachment& blend(VkBool32);
		Attachment& write_mask(VkColorComponentFlags);
		Attachment& blend_color(VkBlendOp, VkBlendFactor, VkBlendFactor);
		Attachment& blend_alpha(VkBlendOp, VkBlendFactor, VkBlendFactor);
		VkPipelineColorBlendAttachmentState state() const;
	private:
		VkPipelineColorBlendAttachmentState state_;
	};

	ColorBlendState();
	~ColorBlendState();

	ColorBlendState(const ColorBlendState&);
	ColorBlendState(ColorBlendState&&);

	ColorBlendState& operator=(const ColorBlendState&);
	ColorBlendState& operator=(ColorBlendState&&);

	ColorBlendState& add(const Attachment&);
	ColorBlendState& logic_op(VkLogicOp);
	ColorBlendState& blend_constants(float, float, float, float);
	const VkPipelineColorBlendStateCreateInfo* info() const;

private:
	struct Pimpl;
	Pimpl* p;
};


class VKW_EXPORT Pipeline::DynamicState {
public:
	DynamicState();
	~DynamicState();

	DynamicState(const std::vector<VkDynamicState> &);
	DynamicState(const DynamicState&);
	DynamicState(DynamicState&&);

	DynamicState& operator=(const DynamicState&);
	DynamicState& operator=(DynamicState&&);

	const VkPipelineDynamicStateCreateInfo* info() const;

private:
	struct Pimpl;
	Pimpl* p;
};


class VKW_EXPORT Pipeline::Configuration {
public:
	Configuration();
	~Configuration();
	Configuration(const Configuration&);
	Configuration(Configuration&&);
	
	Configuration& operator=(const Configuration&);
	Configuration& operator=(Configuration&&);

	Configuration& add_shader(VkShaderModule, VkShaderStageFlagBits);
	VkGraphicsPipelineCreateInfo create_info() const;

	VertexInputState& vertex_input_state();
	InputAssemblyState& input_assembly_state();
	ViewportState& viewport_state();
	RasterizationState& rasterization_state();
	MultisampleState& multisample_state();
	DepthStencilState& depth_stencil_state();
	ColorBlendState& color_blend_state();
	DynamicState& dynamic_state();

	VkPipelineCreateFlags flags() const;
	VkPipelineLayout layout() const;
	VkRenderPass render_pass() const;
	uint32_t subpass() const;

	Configuration& flags(VkPipelineCreateFlags value);
	Configuration& layout(VkPipelineLayout value);
	Configuration& render_pass(VkRenderPass value);
	Configuration& subpass(uint32_t value);

private:
	struct Pimpl;
	Pimpl* p;
};


class VKW_EXPORT GraphicsPipelines {
public:
	class Factory;

	GraphicsPipelines();
	GraphicsPipelines(const Factory&, VkDevice, VkAllocationCallbacks* = nullptr);
	~GraphicsPipelines();

	GraphicsPipelines(const GraphicsPipelines&) = delete;
	GraphicsPipelines(GraphicsPipelines&&);
	GraphicsPipelines& operator=(const GraphicsPipelines&) = delete;
	GraphicsPipelines& operator=(GraphicsPipelines&&);

	const Pipeline& operator[](const std::string&) const;
private:
	struct Pimpl;
	Pimpl* p;
};


class VKW_EXPORT GraphicsPipelines::Factory {
public:
	Factory();
	~Factory();

	Factory(const Factory& other);
	Factory(Factory&& other);

	Factory& operator=(const Factory& other);
	Factory& operator=(Factory&& other);

	Factory& add(const std::string& name, const Pipeline::Configuration& config);
	Factory& cache(VkPipelineCache);

	std::vector<std::string> pipelines() const;
	const Pipeline::Configuration& operator[](const std::string& name) const;
	Pipeline::Configuration& operator[](const std::string& name);
	VkPipelineCache cache() const;

	GraphicsPipelines create(VkDevice device, VkAllocationCallbacks* = nullptr) const;
private:
	struct Pimpl;
	Pimpl* p;
};

}
