#pragma once

#include <variant>
#include <functional>
#include <stdexcept>

namespace util {
namespace predicate {

template <class T, class...V>
struct contains_type;

template <class T, class U, class...V>
struct contains_type<T, U, V...> {
	static constexpr bool value = std::is_same<T, U>::value? true : contains_type<T, V...>::value;
};

template <class T>
struct contains_type<T> {
	static constexpr bool value = false;
};

template <template <class...> class T, class...Ts>
struct join_type {
	template <class X, class...Xs>
	struct with {
		typedef typename std::conditional<contains_type<X, Ts...>::value,
			typename join_type<T, Ts...>::template with<Xs...>::type,
			typename join_type<T, Ts..., X>::template with<Xs...>::type>::type type;
	};
	template <class X>
	struct with<X> {
		typedef typename std::conditional<contains_type<X, Ts...>::value,
			T<Ts...>,
			T<Ts..., X>>::type type;
	};
};

template <template <class...> class T>
struct join_type<T> {
	template <class X, class...Xs>
	struct with {
		typedef T<X, Xs...> type;
	};
};

template <class...A>
class Predicate {
public:
	typedef std::variant<A...> v_t;

	Predicate(v_t condition):orig_{condition} {

	}

	Predicate(const std::vector<v_t>& conditions):orig_{conditions},bound_{orig_} {

	}

	virtual ~Predicate() {}

	template <class...T>
	auto operator&&(const Predicate<T...>& p) const {
		typedef typename join_type<Predicate, A...>::template with<T...>::type result_t;
		typedef typename join_type<std::variant, A...>::template with<T...>::type value_t;
		std::vector<value_t> values;
		for (auto it = orig_.begin(); it != orig_.end(); ++it) {
			values.push_back(std::visit([](auto&& val) -> value_t { return val; }, *it));
		}
		for (auto it = p.conditions().begin(); it != p.conditions().end(); ++it) {
			values.push_back(std::visit([](auto&& val) -> value_t { return val; }, *it));
		}
		return result_t(values);
	}

	const std::vector<v_t>& conditions() const {
		return orig_;
	}

	template <class...Args>
	bool operator()(Args&&...args) const {
		return std::all_of(
			bound_.begin(),
			bound_.end(),
			[&](auto&& p) -> bool {
				if (!std::holds_alternative<std::function<bool(Args...)>>(p)) {
					throw std::runtime_error("Predicate called with invalid args");
				}
				return std::get<std::function<bool(Args&&...)>>(p)(std::forward<Args>(args)...);
			});
	}

	template <class R, class...Args>
	Predicate bind(Args&&...args) const {
		Predicate result(bound_);
		for (size_t i=0; i<result.orig_.size(); ++i) {
			if (std::holds_alternative<R>(result.orig_[i])) {
				auto func = std::bind(std::get<R>(result.orig_[i]), std::forward<Args>(args)...);
				result.bound_[i] = [=](auto&& x) -> auto { return func(x); };
			}
		}
		return result;
	}

private:
	std::vector<v_t> orig_;
	std::vector<v_t> bound_;
};

}
}
