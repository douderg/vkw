#pragma once
#include <vkw_export.h>
#include <vkw/command_pool.hpp>
#include <vulkan/vulkan.hpp>

namespace vkw {


class VKW_EXPORT SubmitQueue {
public:
    SubmitQueue(VkQueue = VK_NULL_HANDLE);
    SubmitQueue& queue(VkQueue);
    ~SubmitQueue();

    SubmitQueue(const SubmitQueue&) = delete;
    SubmitQueue& operator=(const SubmitQueue&) = delete;

    SubmitQueue(SubmitQueue&& other);
    SubmitQueue& operator=(SubmitQueue&& other);

    VkQueue queue() const;
    SubmitQueue& wait_for(VkSemaphore, VkPipelineStageFlags);
    SubmitQueue& will_unblock(VkSemaphore);
    SubmitQueue& will_unblock(const std::vector<VkSemaphore> &);
    SubmitQueue& will_unblock(VkFence);
    SubmitQueue& command_buffer(VkCommandBuffer);
    SubmitQueue& command_buffers(const std::vector<VkCommandBuffer> &);
    VkResult submit() const;

private:
    struct Pimpl;
    Pimpl* p; 
};


class VKW_EXPORT SubmitQueueGuard {
public:
    SubmitQueueGuard(VkQueue queue = VK_NULL_HANDLE);
    ~SubmitQueueGuard();
    SubmitQueue& submit_queue();
private:
    SubmitQueue submit_queue_;
};


class VKW_EXPORT QueueWaitIdleGuard {
public:
    QueueWaitIdleGuard();
    ~QueueWaitIdleGuard();
    QueueWaitIdleGuard& queue(VkQueue);
private:
    VkQueue queue_;
};


class VKW_EXPORT SubmitCommands {
public:
    SubmitCommands(VkDevice device, uint32_t queue_family_index, uint32_t queue_index);
    ~SubmitCommands() = default;
    CommandBufferBegin begin();
private:
    VkDevice device_;
    uint32_t queue_family_;
	VkQueue queue_;
	vkw::CommandPool pool_;
	vkw::CommandPool::Allocation alloc_;
    QueueWaitIdleGuard wait_finish_;
    SubmitQueueGuard submit_;
    QueueWaitIdleGuard wait_start_;
};


class VKW_EXPORT PresentQueue {
public:

    PresentQueue(VkQueue);
    ~PresentQueue();

    PresentQueue(const PresentQueue& other) = delete;
    PresentQueue& operator=(const PresentQueue& other) = delete;

    PresentQueue(PresentQueue&& other);
    PresentQueue& operator=(PresentQueue&& other);

    PresentQueue& waits(VkSemaphore);
    PresentQueue& waits(const std::vector<VkSemaphore> &);
    PresentQueue& swapchain(VkSwapchainKHR);
    PresentQueue& swapchains(const std::vector<VkSwapchainKHR> &);
    PresentQueue& image(uint32_t);
    PresentQueue& images(const std::vector<uint32_t> &);
    VkResult present() const;

private:
    struct Pimpl;
    Pimpl* p;
};

class VKW_EXPORT Semaphore {
public:
    Semaphore();
    Semaphore(VkDevice);
    ~Semaphore();

    Semaphore(const Semaphore&) = delete;
    Semaphore(Semaphore&&);

    Semaphore& operator=(const Semaphore&) = delete;
    Semaphore& operator=(Semaphore&&);

    VkSemaphore handle() const;

private:
    VkDevice device_;
    VkSemaphore handle_;
};

class VKW_EXPORT Fence {
public:
    Fence();
    Fence(VkDevice, VkFenceCreateFlags);
    ~Fence();

    Fence(const Fence&) = delete;
    Fence(Fence&&);

    Fence& operator=(const Fence&) = delete;
    Fence& operator=(Fence&&);

    VkFence handle() const;
    void wait(uint64_t timeout) const;
    void reset();

private:
    VkDevice device_;
    VkFence handle_;
};

}
