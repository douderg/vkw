#ifndef _RENDER_PASS_HPP_
#define _RENDER_PASS_HPP_

#include "vkw_export.h"
#include <vulkan/vulkan.hpp>
#include <vector>

namespace vkw {

class VKW_EXPORT RenderPass {
public:
	class VKW_EXPORT Configuration {
	public:
		virtual ~Configuration();
		virtual std::vector<VkAttachmentDescription> attachments() const = 0;
		virtual std::vector<VkSubpassDescription> subpasses() const = 0;
		virtual std::vector<VkSubpassDependency> subpass_dependencies() const = 0;
	};

	class Factory;

	class VKW_EXPORT Commands {
	public:
		Commands(VkCommandBuffer, const VkRenderPassBeginInfo*, VkSubpassContents);
		~Commands();
		
		template <class CommandFunc, class...Args> 
		void add(CommandFunc&& func, Args&&...args) const {
			func(buffer_, std::forward(args)...);
		}
		
	private:
		VkCommandBuffer buffer_;
	};

	RenderPass();
	RenderPass(const Configuration&, VkDevice, VkAllocationCallbacks* = nullptr);
	~RenderPass();
	RenderPass(const RenderPass&) = delete;
	RenderPass(RenderPass&&);
	RenderPass& operator=(const RenderPass&) = delete;
	RenderPass& operator=(RenderPass&&);
	VkRenderPass handle() const;

	Commands begin(VkCommandBuffer, VkFramebuffer, VkRect2D, const std::vector<VkClearValue>&) const;

private:
	VkDevice device_;
	VkRenderPass handle_;
	VkAllocationCallbacks* alloc_callbacks_;
};

class VKW_EXPORT Attachment {
public:
	Attachment(VkFormat format);
	Attachment& initial_layout(VkImageLayout layout);
	Attachment& final_layout(VkImageLayout layout);
	VkAttachmentDescription description() const;
private:
	VkAttachmentDescription info_;
};

class VKW_EXPORT Subpass {
public:
	Subpass(VkPipelineBindPoint bind_point);
	~Subpass();

	Subpass(const Subpass&) = delete;
	Subpass& operator=(const Subpass&) = delete;

	Subpass(Subpass&& other);
	Subpass& operator=(Subpass&& other);

	Subpass& input_attachments(const std::vector<VkAttachmentReference>& value);
	Subpass& color_attachments(const std::vector<VkAttachmentReference>& value);
	Subpass& preserve_attachments(const std::vector<uint32_t>& value);
	Subpass& depth_stencil_attachment(VkAttachmentReference value);

	VkSubpassDescription description() const;

private:
	struct Pimpl;
	Pimpl *p;
	VkPipelineBindPoint bind_point_;
};

class VKW_EXPORT SubpassDependency {
public:
	uint32_t subpass_src() const;
	uint32_t subpass_dst() const;
	VkPipelineStageFlags stage_mask_src() const;
	VkPipelineStageFlags stage_mask_dst() const;
	VkAccessFlags access_src() const;
	VkAccessFlags access_dst() const;
	VkDependencyFlags dependency() const;

	VkSubpassDependency description() const;

	SubpassDependency& dependency(VkDependencyFlags flags);
	SubpassDependency& subpasses(uint32_t src, uint32_t dst);
	SubpassDependency& stage_masks(VkPipelineStageFlags src, VkPipelineStageFlags dst);
	SubpassDependency& access_masks(VkAccessFlags src, VkAccessFlags dst);

	SubpassDependency& source(uint32_t subpass, VkPipelineStageFlags stage_mask, VkAccessFlags access_mask);
	SubpassDependency& destination(uint32_t subpass, VkPipelineStageFlags stage_mask, VkAccessFlags access_mask);

private:
	VkSubpassDependency info_;
};

class VKW_EXPORT RenderPass::Factory : public RenderPass::Configuration {
public:
	Factory();
	~Factory();

	Factory(const Factory&) = delete;
	Factory& operator=(const Factory&) = delete;

	std::vector<VkAttachmentDescription> attachments() const;
	std::vector<VkSubpassDescription> subpasses() const;
	std::vector<VkSubpassDependency> subpass_dependencies() const;

	Factory& attachments(const std::vector<Attachment>& values);
	Factory& subpasses(const std::vector<Subpass>& values);
	Factory& subpass_dependencies(const std::vector<SubpassDependency>& values);
	RenderPass create(VkDevice device, VkAllocationCallbacks* = nullptr);
private:

	struct Pimpl;
	Pimpl* p;
};

}

#endif