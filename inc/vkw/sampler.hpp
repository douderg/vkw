#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.hpp>

namespace vkw {

class VKW_EXPORT Sampler {
public:
    class Factory;

    Sampler();
    Sampler(VkDevice device, VkSampler sampler, VkAllocationCallbacks* = nullptr);
    ~Sampler();

    Sampler(const Sampler&) = delete;
    Sampler(Sampler&&);

    Sampler& operator=(const Sampler&) = delete;
    Sampler& operator=(Sampler&&);

    VkSampler handle() const;
private:
    VkDevice device_;
    VkSampler handle_;
    VkAllocationCallbacks* alloc_callbacks_;
};


class VKW_EXPORT Sampler::Factory {
public:
    Factory();
    Sampler create(VkDevice device, VkAllocationCallbacks* callbacks = nullptr) const;
private:
    VkSamplerCreateInfo info_;
};

}
