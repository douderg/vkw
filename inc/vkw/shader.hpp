#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.hpp>
#include <filesystem>

namespace vkw {

class VKW_EXPORT ShaderModule {
public:
	class Builder;

	ShaderModule();
	ShaderModule(VkDevice, const std::filesystem::path&, VkAllocationCallbacks* = nullptr);
	ShaderModule(VkDevice, uint32_t* data, size_t bytes, VkAllocationCallbacks* = nullptr);
	ShaderModule(const ShaderModule&) = delete;
	ShaderModule(ShaderModule&&);
	~ShaderModule();

	ShaderModule& operator=(const ShaderModule&) = delete;
	ShaderModule& operator=(ShaderModule&&);

	VkShaderModule handle() const;

private:
	ShaderModule(VkShaderModule, VkDevice, VkAllocationCallbacks* = nullptr);

	VkDevice device_;
	VkShaderModule handle_;
	VkAllocationCallbacks* alloc_callbacks_;
};

}
