#pragma once

#include <vkw_export.h>
#ifdef VK_USE_PLATFORM_XCB_KHR
#include <xcb/xcb.h>
#endif

#ifdef VKW_USE_SDL
#include <SDL.h>
#endif

#include <vulkan/vulkan.h>
#include <vector>

namespace vkw {

class VKW_EXPORT Surface {
public:
	Surface();

#ifdef VK_USE_PLATFORM_XCB_KHR
	Surface(VkInstance, xcb_connection_t*, xcb_window_t, VkAllocationCallbacks* = nullptr);
#endif

#ifdef VK_USE_PLATFORM_WIN32_KHR
	Surface(VkInstance, HINSTANCE, HWND, VkAllocationCallbacks* = nullptr);
#endif

#ifdef VK_USE_PLATFORM_XLIB_KHR
	Surface(VkInstance, Display*, Window, VkAllocationCallbacks* = nullptr);
#endif

#ifdef VKW_USE_SDL
	Surface(VkInstance, SDL_Window*, VkAllocationCallbacks* = nullptr);
#endif

	~Surface();
	Surface(const Surface&) = delete;
	Surface& operator=(const Surface&) = delete;
	Surface(Surface&& other);
	Surface& operator=(Surface&& other);
	
	std::vector<VkSurfaceFormatKHR> supported_formats(VkPhysicalDevice) const;
	std::vector<VkPresentModeKHR> present_modes(VkPhysicalDevice) const;
	VkSurfaceCapabilitiesKHR capabilities(VkPhysicalDevice) const;
	VkSurfaceKHR handle() const;

	static std::vector<const char*> required_extensions();

	VkSurfaceFormatKHR select_format(VkPhysicalDevice device, const std::vector<VkSurfaceFormatKHR>& formats) const;

private:
	VkInstance instance_;
	VkSurfaceKHR handle_;
	const VkAllocationCallbacks* alloc_callbacks_;
};

}
