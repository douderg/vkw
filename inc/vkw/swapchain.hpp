#pragma once

#include <vkw_export.h>
#include <vkw/surface.hpp>
#include <vkw/image_view.hpp>
#include <vkw/framebuffer.hpp>
#include <vkw/queues.hpp>
#include <vulkan/vulkan.hpp>
#include <vector>

namespace vkw {

class VKW_EXPORT Swapchain {
public:
	class Configuration {
	public:
		virtual VkSurfaceKHR surface() const = 0;
		virtual VkSurfaceCapabilitiesKHR surface_capabilities() const = 0;
		virtual VkSurfaceFormatKHR image_format() const = 0;
		virtual VkImageUsageFlagBits image_usage() const = 0;
		virtual uint32_t image_layers() const = 0;
		virtual VkSharingMode sharing_mode() const = 0;
		virtual VkExtent2D extent() const = 0;
		virtual VkPresentModeKHR present_mode() const = 0;
		virtual VkCompositeAlphaFlagBitsKHR composite_alpha() const = 0;
		virtual VkBool32 is_clipped() const = 0;
		virtual const std::vector<uint32_t>& queue_indices() const = 0;
	};

	class Factory;

	Swapchain(VkDevice = VK_NULL_HANDLE, VkAllocationCallbacks* = nullptr);
	Swapchain(VkDevice device, const Configuration& cfg, VkRenderPass render_pass, VkAllocationCallbacks* = nullptr);
	Swapchain(VkDevice device, const Configuration& cfg, VkRenderPass render_pass, VkImageView depth_buffer, VkAllocationCallbacks* = nullptr);
	~Swapchain();
	Swapchain(Swapchain&&);
	Swapchain(const Swapchain&) = delete;
	Swapchain& operator=(Swapchain&&);
	Swapchain& operator=(const Swapchain&) = delete;

	Swapchain& create(const Configuration&, VkRenderPass);
	Swapchain& create(const Configuration&, VkRenderPass, VkImageView);

	size_t image_count() const;
	
	VkFramebuffer framebuffer(size_t index) const;

	VkSwapchainKHR handle() const;

	VkResult acquire_next_image(uint32_t* result, VkSemaphore lock, size_t timeout) const;

private:
	std::vector<VkImage> get_images() const;

	struct Pimpl;
	Pimpl* p;

	VkDevice device_;
	VkSwapchainKHR handle_;
	VkAllocationCallbacks* alloc_callbacks_;
};

class VKW_EXPORT Swapchain::Factory : public Swapchain::Configuration {
public:
	Factory();
	~Factory();

	Factory(const Factory& other) = delete;
	Factory& operator=(const Factory& other) = delete;

	VkSurfaceKHR surface() const override;
	VkSurfaceCapabilitiesKHR surface_capabilities() const override;
	VkSurfaceFormatKHR image_format() const override;
	VkImageUsageFlagBits image_usage() const override;
	uint32_t image_layers() const override;
	VkSharingMode sharing_mode() const override;
	VkExtent2D extent() const override;
	VkPresentModeKHR present_mode() const override;
	VkCompositeAlphaFlagBitsKHR composite_alpha() const override;
	VkBool32 is_clipped() const override;
	const std::vector<uint32_t>& queue_indices() const override;

	Factory& surface(VkSurfaceKHR surface);
	Factory& surface_capabilities(VkSurfaceCapabilitiesKHR capabilities);
	Factory& image_format(VkSurfaceFormatKHR format);
	Factory& image_usage(VkImageUsageFlagBits value);
	Factory& image_layers(uint32_t value);
	Factory& sharing_mode(VkSharingMode value);
	Factory& extent(const VkExtent2D& value);
	Factory& present_mode(VkPresentModeKHR value);
	Factory& composite_alpha(VkCompositeAlphaFlagBitsKHR value);
	Factory& is_clipped(VkBool32 value);
	Factory& queue_indices(const std::vector<uint32_t>& indices);

	Swapchain create(VkDevice device, VkRenderPass render_pass);
	Swapchain create(VkDevice device, VkRenderPass render_pass, VkImageView depth_buffer);
private:
	struct Pimpl;
	Pimpl *p;

	VkSurfaceKHR surface_;
	VkSurfaceCapabilitiesKHR capabilities_;
	VkSurfaceFormatKHR format_;
	VkImageUsageFlagBits image_usage_;
	uint32_t layers_;
	VkSharingMode sharing_mode_;
	VkExtent2D extent_;
	VkPresentModeKHR present_mode_;
	VkCompositeAlphaFlagBitsKHR composite_alpha_;
	VkBool32 clipped_;
	// std::vector<uint32_t> queue_indices_;
};

}
