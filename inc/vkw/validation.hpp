#pragma once

#include <vkw_export.h>
#include <vulkan/vulkan.hpp>

namespace vkw {

class VKW_EXPORT DebugMessenger {
public:
    DebugMessenger();
    DebugMessenger(VkInstance instance);
    ~DebugMessenger();

    DebugMessenger(const DebugMessenger&) = delete;
    DebugMessenger& operator=(const DebugMessenger&) = delete;

    DebugMessenger(DebugMessenger&&);
    DebugMessenger& operator=(DebugMessenger&&);

private:
    VkInstance instance_;
    VkDebugUtilsMessengerEXT handle_;
};

}
