#pragma once

#include <vkw_export.h>
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <stack>
#include <numeric>
#include <queue>
#include <set>
#include <iostream>
#include <chrono>
#include <random>

namespace vkw {
class VKW_EXPORT WaveFunctionCollapse {
public:
    WaveFunctionCollapse(size_t node_count);
    void add_profile(const std::map<uint64_t, std::set<uint64_t>>& states);
    void set_available_states(uint64_t node, const std::set<uint64_t>& states);
    void link_nodes(uint64_t src, uint64_t dst, uint64_t profile_index);
    std::vector<uint64_t> evaluate(unsigned int seed = 0);

private:
    struct step_data {
        uint64_t selected_node;
        uint64_t selected_state;
        std::vector<uint64_t> tried_nodes;
        std::map<uint64_t, std::set<uint64_t>> disabled_in_step;
    };

    bool select_state(uint64_t node, uint64_t selected_state, step_data& data);

    bool select_state_mut(uint64_t node, uint64_t selected_state, step_data& data);

    std::vector<std::map<uint64_t, std::vector<size_t>>> links_;
    std::vector<std::map<uint64_t, std::set<uint64_t>>> profiles_;
    std::vector<size_t> entropies_;
    std::vector<std::set<size_t>> available_;
    std::vector<bool> fixed_nodes_;
    std::vector<uint64_t> selected_states_;
};

}
