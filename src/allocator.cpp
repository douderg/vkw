#include <vkw/allocator.hpp>
#include <vkw/buffer.hpp>
#include <vkw/image.hpp>
#include <vkw/depth_buffer.hpp>
#include <stdexcept>

namespace vkw {

Allocator::Allocator():handle_{VK_NULL_HANDLE} {

}

Allocator::Allocator(VkPhysicalDevice physical_device, VkDevice device):handle_{VK_NULL_HANDLE} {
    VmaAllocatorCreateInfo info = {};
    info.physicalDevice = physical_device;
    info.device = device;
    if (vmaCreateAllocator(&info, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create allocator");
    }
}

Allocator::~Allocator() {
    if (handle_ != VK_NULL_HANDLE) {
        vmaDestroyAllocator(handle_);
        handle_ = VK_NULL_HANDLE;
    }
}

Allocator::Allocator(Allocator&& other):handle_{other.handle_} {
    other.handle_ = VK_NULL_HANDLE;
}

Allocator& Allocator::operator=(Allocator&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vmaDestroyAllocator(handle_);
    }
    handle_ = other.handle_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

VmaAllocator Allocator::handle() const {
    return handle_;
}

Buffer Allocator::alloc_cpu(VkDeviceSize size, VkBufferUsageFlags usage) const {
    return Buffer(handle_, size, usage, VMA_MEMORY_USAGE_CPU_ONLY);
}

Buffer Allocator::alloc_gpu(VkDeviceSize size, VkBufferUsageFlags usage) const {
    return Buffer(handle_, size, usage, VMA_MEMORY_USAGE_GPU_ONLY);
}

Image Allocator::alloc_img(const Image::Configuration& cfg) const {
    return Image(cfg, handle_, VMA_MEMORY_USAGE_GPU_ONLY);
}

DepthBuffer Allocator::create_depth_buffer(VkExtent2D extent, uint32_t queue_family_idx, uint32_t queue_idx) const {
    return DepthBuffer(handle_, extent, queue_family_idx, queue_idx);
}

}