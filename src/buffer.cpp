#include <vkw/buffer.hpp>
#include <vkw/queues.hpp>
#include <vkw/allocator.hpp>
#include <utility>
#include <stdexcept>

namespace vkw {

Buffer::Buffer():handle_{VK_NULL_HANDLE} {

}

Buffer::Buffer(VmaAllocator allocator, VkDeviceSize size, VkBufferUsageFlags flags, VmaMemoryUsage mem_usage): allocator_{allocator}, handle_{VK_NULL_HANDLE} {
    VkBufferCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    info.size = size;
    info.usage = flags;
    requested_size_ = size;

    VmaAllocationCreateInfo alloc_info = {};
    alloc_info.usage = mem_usage;
    if (vmaCreateBuffer(allocator_, &info, &alloc_info, &handle_, &allocation_, nullptr) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create buffer");
    }
}

Buffer::~Buffer() {
    if (handle_ != VK_NULL_HANDLE) {
        vmaDestroyBuffer(allocator_, handle_, allocation_);
        handle_ = VK_NULL_HANDLE;
    }
}

Buffer::Buffer(Buffer&& other):allocator_{other.allocator_}, handle_{other.handle_}, allocation_{other.allocation_}  {
    other.handle_ = VK_NULL_HANDLE;
}

Buffer& Buffer::operator=(Buffer&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vmaDestroyBuffer(allocator_, handle_, allocation_);
    }
    allocator_ = std::move(other.allocator_);
    handle_ = std::move(other.handle_);
    allocation_ = std::move(other.allocation_);
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

VkBuffer Buffer::handle() const {
    return handle_;
}

VkDeviceSize Buffer::offset() const {
    VmaAllocationInfo info;
    vmaGetAllocationInfo(allocator_, allocation_, &info);
    return info.offset;
}

Buffer& Buffer::load(const void* source, size_t size) {
    void *address;
    if (vmaMapMemory(allocator_, allocation_, &address) != VK_SUCCESS) {
        vmaUnmapMemory(allocator_, allocation_);
        throw std::runtime_error("failed to map memory");
    }
    std::memcpy(address, source, size);
    vmaUnmapMemory(allocator_, allocation_);
    return *this;
}

Buffer Buffer::transfer(uint32_t submit_queue, VkBufferUsageFlags flags) const {
    VkBufferCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    info.size = requested_size_;
    info.usage = flags | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

    VmaAllocationCreateInfo new_alloc_info = {};
    new_alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    Buffer result;
    result.allocator_ = allocator_;
    if (vmaCreateBuffer(allocator_, &info, &new_alloc_info, &result.handle_, &result.allocation_, nullptr) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create buffer");
    }

    vkw::SubmitCommands guard(get_allocator_device(allocator_), submit_queue, 0);
    auto begin = guard.begin();
    VkBufferCopy region = {};
    region.size = requested_size_;
    region.srcOffset = 0;
    region.dstOffset = 0;
    vkCmdCopyBuffer(begin.handle(), handle_, result.handle_, 1, &region);
    return result;
}


Image Buffer::create_image(uint32_t submit_queue, const Image::Factory& factory, VkImageLayout image_layout, VkPipelineStageFlags stage_flags, VkAccessFlags access_mask, VkImageAspectFlags flags) const {
    Image result = factory.create(allocator_, VMA_MEMORY_USAGE_GPU_ONLY);
    {vkw::SubmitCommands guard(get_allocator_device(allocator_), submit_queue, 0);
    auto cmd_buffer = guard.begin();
    result.change_layout(cmd_buffer.handle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_ACCESS_TRANSFER_WRITE_BIT, flags);}
    {vkw::SubmitCommands guard(get_allocator_device(allocator_), submit_queue, 0);
    auto cmd_buffer = guard.begin();
    VkBufferImageCopy region{};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = flags;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset = {0, 0, 0};
    region.imageExtent = factory.extent();
    vkCmdCopyBufferToImage(cmd_buffer.handle(), handle_, result.handle(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);}
    {vkw::SubmitCommands guard(get_allocator_device(allocator_), submit_queue, 0);
    auto cmd_buffer = guard.begin();
    result.change_layout(cmd_buffer.handle(), image_layout, stage_flags, access_mask, flags);}
    return result;
}


}