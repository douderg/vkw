#include <vkw/command_pool.hpp>

namespace vkw {

CommandBufferBegin::CommandBufferBegin(VkCommandBuffer buffer, VkCommandBufferUsageFlags flags):
		handle_{buffer} {

	VkCommandBufferBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.flags = flags;
	if (handle_ != VK_NULL_HANDLE) {
		vkBeginCommandBuffer(handle_, &info);
	}
}

CommandBufferBegin::~CommandBufferBegin() {
	end();
}

CommandBufferBegin::CommandBufferBegin(CommandBufferBegin&& other):handle_{other.handle_} {
	other.handle_ = VK_NULL_HANDLE;
}

CommandBufferBegin& CommandBufferBegin::operator=(CommandBufferBegin&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkEndCommandBuffer(handle_);

	}
	handle_ = other.handle_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}

VkCommandBuffer CommandBufferBegin::handle() const {
	return handle_;
}

VkResult CommandBufferBegin::end() {
	VkResult result = VK_SUCCESS;
	if (handle_ != VK_NULL_HANDLE) {
		result = vkEndCommandBuffer(handle_);
		handle_ = VK_NULL_HANDLE;
	}
	return result;
}

CommandPool::CommandPool():
		device_{VK_NULL_HANDLE}, 
		handle_{VK_NULL_HANDLE}, 
		alloc_callbacks_{nullptr} {

}

CommandPool::CommandPool(VkDevice device, uint32_t queue_index, VkAllocationCallbacks* callbacks):
		device_{device}, 
		handle_{VK_NULL_HANDLE}, 
		alloc_callbacks_{callbacks} {

	VkCommandPoolCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	info.queueFamilyIndex = queue_index;
	if (vkCreateCommandPool(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create command pool");
	}
}

CommandPool::CommandPool(VkDevice device, uint32_t queue_index, VkCommandPoolCreateFlags flags, VkAllocationCallbacks* callbacks):
		device_{device}, 
		handle_{VK_NULL_HANDLE}, 
		alloc_callbacks_{callbacks} {
	VkCommandPoolCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	info.queueFamilyIndex = queue_index;
	info.flags = flags;
	if (vkCreateCommandPool(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create command pool");
	}
}

CommandPool::CommandPool(CommandPool&& other):
		device_{other.device_}, 
		handle_{other.handle_}, 
		alloc_callbacks_{other.alloc_callbacks_} {
	other.handle_ = VK_NULL_HANDLE;
}

CommandPool::~CommandPool() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyCommandPool(device_, handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}

CommandPool& CommandPool::operator=(CommandPool&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyCommandPool(device_, handle_, alloc_callbacks_);
	}
	handle_ = other.handle_;
	device_ = other.device_;
	alloc_callbacks_ = other.alloc_callbacks_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}

CommandPool::Allocation CommandPool::allocate(size_t buffer_count, VkCommandBufferLevel level) {
	return Allocation(device_, handle_, buffer_count, level);
}

struct CommandPool::Allocation::Pimpl {
	VkDevice device;
	VkCommandPool pool;
	std::vector<VkCommandBuffer> buffers;
};

CommandPool::Allocation::Allocation(VkDevice device, VkCommandPool pool, size_t count, VkCommandBufferLevel level):
	p{nullptr}
{
	p = new Pimpl();
	p->device = device;
	p->pool = pool;
	p->buffers.resize(count, VK_NULL_HANDLE);

	VkCommandBufferAllocateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	info.commandPool = p->pool;
	info.level = level;
	info.commandBufferCount = uint32_t(count);
	if (vkAllocateCommandBuffers(p->device, &info, p->buffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to allocate command buffers");
	}
}

CommandPool::Allocation::Allocation():
	p{nullptr}
{
	p = new Pimpl();
	p->device = VK_NULL_HANDLE;
	p->pool = VK_NULL_HANDLE;
}

CommandPool::Allocation::~Allocation() {
	if (p) {
		if (!p->buffers.empty()) {
			vkFreeCommandBuffers(p->device, p->pool, uint32_t(p->buffers.size()), p->buffers.data());
		}
		delete p;
		p = nullptr;
	}
}

CommandPool::Allocation::Allocation(Allocation&& other):
	p{other.p}
{
	other.p = nullptr;
}

CommandPool::Allocation& CommandPool::Allocation::operator=(Allocation&& other) {
	if (p) {
		if (!p->buffers.empty()) {
			vkFreeCommandBuffers(p->device, p->pool, uint32_t(p->buffers.size()), p->buffers.data());
		}
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}

CommandBufferBegin CommandPool::Allocation::begin(size_t index, VkCommandBufferUsageFlags flags) const {
	return CommandBufferBegin(p->buffers[index], flags);
}

VkCommandBuffer CommandPool::Allocation::operator[](size_t index) const {
	return p->buffers[index];
}

size_t CommandPool::Allocation::size() const {
	return p->buffers.size();
}

}