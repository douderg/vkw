#include <vkw/context.hpp>
#include <vkw/allocator.hpp>

namespace vkw {

#if defined(VKW_USE_SDL)
#include <SDL_vulkan.h>
#endif

struct Context::Pimpl {
    Pimpl();
#if defined(VKW_USE_SDL)
    Pimpl(SDL_Window* window);
#endif
    ~Pimpl() = default;

    vkw::Instance instance;
    vkw::Device device;
    vkw::Surface surface;
    vkw::DebugMessenger dbg;
    std::vector<uint32_t> queue_families;
    vkw::Allocator allocator;
};


Context::Pimpl::Pimpl() 
{
    std::vector<const char *> extensions = vkw::Surface::required_extensions();
#ifdef _DEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    auto instance_cfg = vkw::Instance::Configuration()
#ifdef _DEBUG
		.layers({"VK_LAYER_KHRONOS_validation"})
#endif
		.extensions(extensions);
    instance = vkw::Instance(instance_cfg);
#ifdef _DEBUG
    //dbg = vkw::DebugMessenger(instance.handle());
#endif
}


#if defined(VKW_USE_SDL)
Context::Pimpl::Pimpl(SDL_Window* window) {
    unsigned int count;
    if (!SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr)) {
        throw std::runtime_error("error: failed to retrieve required extensions");
    }
    std::vector<const char*> extensions(count);
    if (!SDL_Vulkan_GetInstanceExtensions(window, &count, extensions.data())) {
        throw std::runtime_error("error: failed to retrieve required extension names");
    }
#ifdef _DEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif
    auto instance_cfg = vkw::Instance::Configuration()
#ifdef _DEBUG
		.layers({"VK_LAYER_KHRONOS_validation"})
#endif
		.extensions(extensions);
    instance = vkw::Instance(instance_cfg);
#ifdef _DEBUG
    //dbg = vkw::DebugMessenger(instance.handle());
#endif
    surface = vkw::Surface(instance.handle(), window);
}


Context::Context(SDL_Window* window):
    p{nullptr}
{
    p = new Pimpl(window);
}
#endif


Context::Context():
    p{nullptr} 
{
    p = new Pimpl();
}


Context::~Context() {
    if (p) {
        delete p;
        p = nullptr;
    }
}


Context::Context(Context&& other) {
    p = other.p;
    other.p = nullptr;
}


Context& Context::operator=(Context&& other) {
    if (p) {
        delete p;
    }
    p = other.p;
    other.p = nullptr;
    return *this;
}


#if defined(VK_USE_PLATFORM_WIN32_KHR)
void Context::create_surface(HINSTANCE instance, HWND hwnd) {
    p->surface = vkw::Surface(p->instance.handle(), instance, hwnd);
}
#endif


#if defined(VK_USE_PLATFORM_XLIB_KHR)
void Context::create_surface(Display* display, Window window) {
    p->surface = vkw::Surface(p->instance.handle(), display, window);
}
#endif


void Context::create_device(VkPhysicalDevice device, Device::Configuration& config) {
    p->device = config.create(p->instance.handle(), device);
    device = config.device();
    p->queue_families = config.queue_family_indices();
    p->allocator = std::move(vkw::Allocator(config.device(), p->device.handle()));
}


VkInstance Context::instance() const {
    return p->instance.handle();
}


Device& Context::device() {
    return p->device;
}


const Device& Context::device() const {
    return p->device;
}


Allocator& Context::allocator() {
    return p->allocator;
}


const Allocator& Context::allocator() const {
    return p->allocator;
}


Surface& Context::surface() {
    return p->surface;
}


const Surface& Context::surface() const {
    return p->surface;
}


uint32_t Context::get_queue_family_index(size_t position) const {
    return p->queue_families.at(position);
}

}
