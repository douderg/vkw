#include <vkw/depth_buffer.hpp>
#include <vkw/allocator.hpp>

namespace vkw {

class DepthBufferConfig : public Image::Configuration {
public:
    DepthBufferConfig(VkExtent3D extent): extent_{extent} {}
    ~DepthBufferConfig() = default;
    VkImageType type() const override { return VK_IMAGE_TYPE_2D; }
    VkFormat format() const override { return VK_FORMAT_D32_SFLOAT_S8_UINT; }
    VkExtent3D extent() const override { return extent_; }
    uint32_t mip_levels() const override { return 1; }
    uint32_t layers() const override { return 1; }
    VkSampleCountFlagBits samples() const override { return VK_SAMPLE_COUNT_1_BIT; }
    VkImageTiling tiling() const override { return VK_IMAGE_TILING_OPTIMAL; }
    VkImageUsageFlags usage() const override { return VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT; }
public:
    VkExtent3D extent_;
};


DepthBuffer::DepthBuffer(VmaAllocator allocator, VkExtent2D extent, uint32_t queue_family, uint32_t queue_idx):
        image_{DepthBufferConfig({extent.width, extent.height, 1}), allocator, VMA_MEMORY_USAGE_GPU_ONLY},
        view_{image_.create_view(VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT)} {
    
    vkw::SubmitCommands submit_commands{get_allocator_device(allocator), queue_family, queue_idx};
    auto submit_guard = submit_commands.begin(); 
    image_.change_layout(
        submit_guard.handle(),
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
        VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT
    );
}

DepthBuffer::DepthBuffer(DepthBuffer&& other):
        image_{std::move(other.image_)},
        view_{std::move(other.view_)} {

}

DepthBuffer& DepthBuffer::operator=(DepthBuffer&& other) {
    image_ = std::move(other.image_);
    view_ = std::move(other.view_);
    return *this;
}

VkImage DepthBuffer::image() const {
    return image_.handle();
}

VkImageView DepthBuffer::view() const {
    return view_.handle();
}

}