#include "vkw/descriptor_pool.hpp"

namespace vkw {


DescriptorPool::DescriptorPool(): handle_{VK_NULL_HANDLE} {

}


DescriptorPool::DescriptorPool(VkDevice device, uint32_t max_sets, const std::vector<VkDescriptorPoolSize>& sizes, VkAllocationCallbacks* alloc_callbacks):
        device_{device},
        handle_{VK_NULL_HANDLE},
        alloc_callbacks_{alloc_callbacks} {

    VkDescriptorPoolCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    info.poolSizeCount = uint32_t(sizes.size());
    info.pPoolSizes = sizes.empty()? nullptr : sizes.data();
    info.maxSets = max_sets;

    if (vkCreateDescriptorPool(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create descriptor pool");
    }
}


DescriptorPool::~DescriptorPool() {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyDescriptorPool(device_, handle_, alloc_callbacks_);
        handle_ = VK_NULL_HANDLE;
    }
}


DescriptorPool::DescriptorPool(DescriptorPool&& other):device_{other.device_}, handle_{other.handle_}, alloc_callbacks_{other.alloc_callbacks_} {
    other.handle_ = VK_NULL_HANDLE;
}


DescriptorPool& DescriptorPool::operator=(DescriptorPool&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyDescriptorPool(device_, handle_, alloc_callbacks_);
    }
    device_ = other.device_;
    handle_ = other.handle_;
    alloc_callbacks_ = other.alloc_callbacks_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}


VkDescriptorPool DescriptorPool::handle() const {
    return handle_;
}


DescriptorPool::Allocation DescriptorPool::alloc(VkDescriptorSetLayout layout) const {
    return Allocation(device_, handle_, {layout});
}


DescriptorPool::Allocation DescriptorPool::alloc(const std::vector<VkDescriptorSetLayout>& layouts) const {
    return Allocation(device_, handle_, layouts);
}


VkDevice DescriptorPool::device() const {
    return device_;
}


struct DescriptorPool::Factory::Pimpl {
    std::vector<VkDescriptorPoolSize> sizes;
    uint32_t max_sets;
};


DescriptorPool::Factory::Factory():
    p{nullptr}
{
    p = new Pimpl();
}


DescriptorPool::Factory::~Factory() {
    if (p) {
        delete p;
        p = nullptr;
    }
}


DescriptorPool::Factory::Factory(const Factory& other):
    p{nullptr}
{
    if (other.p) {
        p = new Pimpl(*other.p);
    }
}


DescriptorPool::Factory& DescriptorPool::Factory::operator=(const Factory& other) {
    if (p) {
        delete p;
    }
    p = other.p ? new Pimpl(*other.p) : nullptr;
    return *this;
}


DescriptorPool::Factory::Factory(Factory&& other):
    p{other.p}
{
    other.p = nullptr;
}


DescriptorPool::Factory& DescriptorPool::Factory::operator=(Factory&& other) {
    if (p) {
        delete p;
    }
    p = other.p;
    other.p = nullptr;
    return *this;
}


DescriptorPool::Factory& DescriptorPool::Factory::add(uint32_t count, VkDescriptorType type) {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool factory");
    }
    VkDescriptorPoolSize size;
    size.descriptorCount = count;
    size.type = type;
    p->sizes.push_back(size);
    return *this;
}


DescriptorPool::Factory& DescriptorPool::Factory::max_sets(uint32_t value) {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool factory");
    }
    p->max_sets = value;
    return *this;
}


DescriptorPool DescriptorPool::Factory::create(VkDevice device, VkAllocationCallbacks *callbacks) const {
    return DescriptorPool(device, p->max_sets, p->sizes, callbacks);
}


struct DescriptorPool::Allocation::Pimpl {
    VkDevice device;
    std::shared_ptr<std::vector<VkDescriptorSet>> sets;
};


DescriptorPool::Allocation::Allocation():
    p{nullptr}
{
    p = new Pimpl();
    p->device = VK_NULL_HANDLE;
    p->sets = std::make_shared<std::vector<VkDescriptorSet>>();
}


DescriptorPool::Allocation::Allocation(VkDevice device, VkDescriptorPool pool, const std::vector<VkDescriptorSetLayout>& layouts):
    p{nullptr} 
{
    p = new Pimpl();
    p->device = device;

    VkDescriptorSetAllocateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    info.descriptorPool = pool;
    info.descriptorSetCount = uint32_t(layouts.size());
    info.pSetLayouts = layouts.empty()? nullptr : layouts.data();

    p->sets = std::make_shared<std::vector<VkDescriptorSet>>(layouts.size());

    if (vkAllocateDescriptorSets(device, &info, p->sets->data()) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate descriptor sets");
    }
}


DescriptorPool::Allocation::~Allocation() {
    if (p) {
        delete p;
        p = nullptr;
    }
}


DescriptorPool::Allocation::Allocation(Allocation&& other):
    p{other.p}
{
    other.p = nullptr;
}


DescriptorPool::Allocation& DescriptorPool::Allocation::operator=(Allocation&& other) {
    if (p) {
        delete p;
    }
    p = other.p;
    other.p = nullptr;
    return *this;
}


DescriptorPool::Allocation::Update DescriptorPool::Allocation::write_set(size_t index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorBufferInfo>& infos, uint32_t start) const {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool allocation");
    }
    Update result(p->device, p->sets);
    result.write_set(index, binding, type, infos, start);
    return result;
}


DescriptorPool::Allocation::Update DescriptorPool::Allocation::write_set(size_t set_index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorImageInfo>& infos, uint32_t start) const {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool allocation");
    }
    Update result(p->device, p->sets);
    result.write_set(set_index, binding, type, infos, start);
    return result;
}


VkDescriptorSet DescriptorPool::Allocation::operator[](size_t index) const {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool allocation");
    }
    return p->sets->at(index);
}


struct DescriptorPool::Allocation::Update::Pimpl {
    VkDevice device;
    std::vector<VkWriteDescriptorSet> writes;
    std::shared_ptr<std::vector<VkDescriptorSet>> sets;
};


DescriptorPool::Allocation::Update::Update(VkDevice device, std::shared_ptr<std::vector<VkDescriptorSet>> sets):
    p{nullptr}
{
    p = new Pimpl();
    p->device = device;
    p->sets = sets;
}


DescriptorPool::Allocation::Update::~Update() {
    if (p) {
        if (p->device != VK_NULL_HANDLE) {
            vkUpdateDescriptorSets(p->device, uint32_t(p->writes.size()), p->writes.empty()? nullptr : p->writes.data(), 0, nullptr);
        }
        delete p;
        p = nullptr;
    }
}


DescriptorPool::Allocation::Update::Update(Update&& other):
    p{other.p}
{
    other.p = nullptr;
}


DescriptorPool::Allocation::Update& DescriptorPool::Allocation::Update::operator=(Update&& other) {
    if (p) {
        if (p->device != VK_NULL_HANDLE) {
            vkUpdateDescriptorSets(p->device, uint32_t(p->writes.size()), p->writes.empty()? nullptr : p->writes.data(), 0, nullptr);
        }
        delete p;
    }
    p = other.p;
    other.p = nullptr;
    return *this;
}


DescriptorPool::Allocation::Update& DescriptorPool::Allocation::Update::write_set(size_t index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorBufferInfo>& infos, uint32_t start) {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool allocation update");
    }
    VkWriteDescriptorSet write = {};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.dstSet = p->sets->at(index);
    write.dstBinding = binding;
    write.dstArrayElement = start;
    write.descriptorType = type;
    write.descriptorCount = uint32_t(infos.size());
    write.pBufferInfo = infos.empty()? nullptr : infos.data();
    p->writes.push_back(write);
    return *this;
}


DescriptorPool::Allocation::Update& DescriptorPool::Allocation::Update::write_set(size_t index, uint32_t binding, VkDescriptorType type, const std::vector<VkDescriptorImageInfo>& infos, uint32_t start) {
    if (!p) {
        throw std::runtime_error("invalid descriptor pool allocation update");
    }
    VkWriteDescriptorSet write = {};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.dstSet = p->sets->at(index);
    write.dstBinding = binding;
    write.dstArrayElement = start;
    write.descriptorType = type;
    write.descriptorCount = uint32_t(infos.size());
    write.pImageInfo = infos.empty()? nullptr : infos.data();
    p->writes.push_back(write);
    return *this;
}


DescriptorSet::Layout::Layout():
    handle_{VK_NULL_HANDLE},
    device_{VK_NULL_HANDLE},
    alloc_callbacks_{nullptr}
{
}


DescriptorSet::Layout::Layout(VkDevice device, VkDescriptorSetLayoutCreateInfo* info, VkAllocationCallbacks* callbacks):
    device_{device},
    handle_{VK_NULL_HANDLE},
    alloc_callbacks_{callbacks}
{
    if (vkCreateDescriptorSetLayout(device, info, callbacks, &handle_) != VK_SUCCESS) {
        handle_ = VK_NULL_HANDLE;
        throw std::runtime_error("failed to create descriptor set layout");
    }
}


DescriptorSet::Layout::Layout(Layout&& other):
    device_{other.device_},
    handle_{other.handle_},
    alloc_callbacks_{other.alloc_callbacks_}
{
    other.device_ = VK_NULL_HANDLE;
    other.handle_ = VK_NULL_HANDLE;
    other.alloc_callbacks_ = nullptr;
}


DescriptorSet::Layout::~Layout() {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyDescriptorSetLayout(device_, handle_, alloc_callbacks_);
        handle_ = VK_NULL_HANDLE;
    }
}


DescriptorSet::Layout& DescriptorSet::Layout::operator=(Layout&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyDescriptorSetLayout(device_, handle_, alloc_callbacks_);
    }
    device_ = other.device_;
    handle_ = other.handle_;
    alloc_callbacks_ = other.alloc_callbacks_;
    other.device_ = VK_NULL_HANDLE;
    other.handle_ = VK_NULL_HANDLE;
    other.alloc_callbacks_ = nullptr;
    return *this;
}


VkDescriptorSetLayout DescriptorSet::Layout::handle() const {
    return handle_;
}


struct DescriptorSet::Layout::Factory::Pimpl {
    std::vector<DescriptorSet::Layout::Binding> bindings;
};


DescriptorSet::Layout::Factory::Factory():
    p{nullptr}
{
    p = new Pimpl();
}


DescriptorSet::Layout::Factory::~Factory() {
    if (p) {
        delete p;
        p = nullptr;
    }
}


DescriptorSet::Layout::Factory::Factory(const Factory& other):
    p{nullptr}
{
    if (other.p) {
        p = new Pimpl(*other.p);
    }
}


DescriptorSet::Layout::Factory& DescriptorSet::Layout::Factory::operator=(const Factory& other) {
    if (p) {
        delete p;
    }
    p = other.p ? new Pimpl(*other.p) : nullptr;
    return *this;
}


DescriptorSet::Layout::Factory::Factory(Factory&& other):
    p{other.p}
{
    other.p = nullptr;
}


DescriptorSet::Layout::Factory& DescriptorSet::Layout::Factory::operator=(Factory&& other) {
    if (p) {
        delete p;
    }
    p = other.p;
    other.p = nullptr;
    return *this;
}


DescriptorSet::Layout::Factory& DescriptorSet::Layout::Factory::add_binding(const Binding& binding) {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout factory");
    }
    p->bindings.push_back(binding);
    return *this;
}


DescriptorSet::Layout::Factory& DescriptorSet::Layout::Factory::add_binding(uint32_t binding, VkDescriptorType type, VkShaderStageFlags stages, uint32_t count) {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout factory");
    }
    p->bindings.emplace_back(binding).descriptors(count, type).stage_flags(stages);
    return *this;
}


DescriptorSet::Layout DescriptorSet::Layout::Factory::create(VkDevice device, VkAllocationCallbacks* callbacks) {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout factory");
    }
    std::vector<VkDescriptorSetLayoutBinding> bindings(p->bindings.size());
    std::transform(
        p->bindings.begin(),
        p->bindings.end(),
        bindings.begin(),
        [](const Binding& b) -> VkDescriptorSetLayoutBinding { return b.handle(); }
    );

    VkDescriptorSetLayoutCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    info.bindingCount = uint32_t(bindings.size());
    info.pBindings = bindings.empty()? nullptr : bindings.data();

    return Layout(device, &info, callbacks);
}


struct DescriptorSet::Layout::Binding::Pimpl {
    Pimpl();
    VkDescriptorSetLayoutBinding handle;
    std::vector<VkSampler> immutable_samplers;
};


DescriptorSet::Layout::Binding::Pimpl::Pimpl():
    handle{}
{
}


DescriptorSet::Layout::Binding::Binding():
    p{nullptr}
{
    p = new Pimpl();
}


DescriptorSet::Layout::Binding::Binding(uint32_t id):
    p{nullptr}
{
    p = new Pimpl();
    p->handle.binding = id;
}


DescriptorSet::Layout::Binding::~Binding() {
    if (p) {
        delete p;
        p = nullptr;
    }
}


DescriptorSet::Layout::Binding::Binding(const Binding& other):
    p{nullptr}
{
    if (other.p) {
        p = new Pimpl(*other.p);
    }
}


DescriptorSet::Layout::Binding& DescriptorSet::Layout::Binding::operator=(const Binding& other) {
    if (p) {
        delete p;
    }
    p = other.p ? new Pimpl(*other.p) : nullptr;
    return *this;
}


DescriptorSet::Layout::Binding::Binding(Binding&& other):
    p{other.p}
{
    other.p = nullptr;
}


DescriptorSet::Layout::Binding& DescriptorSet::Layout::Binding::operator=(Binding&& other) {
    if (p) {
        delete p;
    }
    p = other.p;
    other.p = nullptr;
    return *this;
}


DescriptorSet::Layout::Binding& DescriptorSet::Layout::Binding::binding(uint32_t id) {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout binding");
    }
    p->handle.binding = id;
    return *this;
}


DescriptorSet::Layout::Binding& DescriptorSet::Layout::Binding::descriptors(uint32_t count, VkDescriptorType type) {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout binding");
    }
    p->handle.descriptorCount = count;
    p->handle.descriptorType = type;
    return *this;
}


DescriptorSet::Layout::Binding& DescriptorSet::Layout::Binding::stage_flags(VkShaderStageFlags flags) {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout binding");
    }
    p->handle.stageFlags = flags;
    return *this;
}


VkDescriptorSetLayoutBinding DescriptorSet::Layout::Binding::handle() const {
    if (!p) {
        throw std::runtime_error("invalid descriptor set layout binding");
    }
    return p->handle;
}


}
