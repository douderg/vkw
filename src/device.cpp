#include <vkw/device.hpp>
#include <vkw/wave_function_collapse.hpp>
#include <algorithm>
#include <numeric>
#include <set>
#include <queue>
#include <chrono>
#include <random>

namespace vkw {

struct Device::Configuration::Pimpl {
	VkPhysicalDevice device;
	std::vector<VkDeviceQueueCreateInfo> queue_infos;
	std::vector<const char*> required_extensions;
	std::vector<const char*> enabled_layers;
	std::vector<VkPhysicalDeviceFeatures> enabled_features;
	std::vector<std::vector<float>> priorities;
	std::vector<VkSurfaceKHR> surfaces;
	std::vector<VkQueueFlags> flags;
	std::function<bool(VkPhysicalDevice, VkPhysicalDevice)> comparator;
};


std::vector<VkPhysicalDevice> get_physical_devices(VkInstance instance) {
	uint32_t count;
	std::vector<VkPhysicalDevice> physical_devices;
	if (vkEnumeratePhysicalDevices(instance, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get physical device count");
	}
	if (count) {
		physical_devices.resize(count);
		if (vkEnumeratePhysicalDevices(instance, &count, physical_devices.data()) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to detect physical devices");
		}
	}
	return physical_devices;
}


std::vector<VkQueueFamilyProperties> queue_family_properties(VkPhysicalDevice device) {
	uint32_t count;
	std::vector<VkQueueFamilyProperties> result;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
	if (count) {
		result.resize(count);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &count, result.data());
	}
	return result;
}


std::vector<VkExtensionProperties> get_available_extensions(VkPhysicalDevice device) {
	uint32_t count = 0;
	if (vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get supported device extension count");
	}
	std::vector<VkExtensionProperties> result;
	if (count) {
		result.resize(count);
		if (vkEnumerateDeviceExtensionProperties(device, nullptr, &count, result.data()) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to get supported device extensions");
		}
	}
	return result;
}


std::vector<VkSurfaceFormatKHR> get_surface_formats(VkPhysicalDevice device, VkSurfaceKHR surface) {
	uint32_t count;
	std::vector<VkSurfaceFormatKHR> result;
	if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get surface formats");
	}
	result.resize(count);
	if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get surface formats");
	}
	return result;
}


std::vector<VkPresentModeKHR> get_present_modes(VkPhysicalDevice device, VkSurfaceKHR surface) {
	uint32_t count;
	std::vector<VkPresentModeKHR> result;
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get present modes");
	}
	result.resize(count);
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get present modes");
	}
	return result;
}


VkSurfaceCapabilitiesKHR get_surface_capabilities(VkPhysicalDevice device, VkSurfaceKHR surface) {
	VkSurfaceCapabilitiesKHR result;
	if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &result) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get surface capabilities");
	}
	return result;
}


Device::Device(): 
	instance_{VK_NULL_HANDLE}, 
	handle_{VK_NULL_HANDLE}, 
	alloc_callbacks_{nullptr},
	device_{VK_NULL_HANDLE} 
{
}


Device::Device(VkInstance instance, const Device::Configuration& cfg, VkAllocationCallbacks* callbacks):
	instance_{instance}, 
	handle_{VK_NULL_HANDLE}, 
	alloc_callbacks_{callbacks},
	device_{VK_NULL_HANDLE}
{
	VkDeviceCreateInfo info{};
	info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	info.queueCreateInfoCount = uint32_t(cfg.queue_infos().size());
	info.pQueueCreateInfos = cfg.queue_infos().empty()? nullptr : cfg.queue_infos().data();
	info.pEnabledFeatures = cfg.enabled_features().empty()? nullptr : cfg.enabled_features().data();
	info.enabledExtensionCount = uint32_t(cfg.required_extensions().size());
	info.ppEnabledExtensionNames = cfg.required_extensions().empty()? nullptr : cfg.required_extensions().data();
	info.enabledLayerCount = uint32_t(cfg.enabled_layers().size());
	info.ppEnabledLayerNames = cfg.enabled_layers().empty()? nullptr : cfg.enabled_layers().data();

	if (vkCreateDevice(cfg.device(), &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Could not create device");
	}

	device_ = cfg.device();
}


Device::~Device() {
	if (handle_ != VK_NULL_HANDLE) {
        vkDeviceWaitIdle(handle_);
		vkDestroyDevice(handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}


Device::Device(Device&& other):
	instance_{other.instance_}, 
	handle_{other.handle_}, 
	alloc_callbacks_{other.alloc_callbacks_},
	device_{other.device_}
{
	other.handle_ = VK_NULL_HANDLE;
}


Device& Device::operator=(Device&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyDevice(handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
	instance_ = other.instance_;
	handle_ = other.handle_;
	alloc_callbacks_ = other.alloc_callbacks_;
	device_ = other.device_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}


VkDevice Device::handle() const {
	return handle_;
}


VkPhysicalDevice Device::physical_device() const {
	return device_;
}


Device::Configuration::Configuration():
	p{nullptr}
{
	p = new Pimpl();
}


Device::Configuration::~Configuration() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


VkPhysicalDevice Device::Configuration::device() const {
	return p->device;
}


const std::vector<VkDeviceQueueCreateInfo>& Device::Configuration::queue_infos() const {
	return p->queue_infos;
}


const std::vector<const char*>& Device::Configuration::required_extensions() const {
	return p->required_extensions;
}


const std::vector<const char*>& Device::Configuration::enabled_layers() const {
	return p->enabled_layers;
}


const std::vector<VkPhysicalDeviceFeatures>& Device::Configuration::enabled_features() const {
	return p->enabled_features;
}


Device::Configuration& Device::Configuration::required_extensions(const std::vector<const char*>& extensions) {
	p->required_extensions = extensions;
	return *this;
}


Device::Configuration& Device::Configuration::enabled_layers(const std::vector<const char*>& layer_names) {
	p->enabled_layers = layer_names;
	return *this;
}


Device::Configuration& Device::Configuration::enabled_features(const std::vector<VkPhysicalDeviceFeatures>& features) {
	p->enabled_features = features;
	return *this;
}


Device::Configuration& Device::Configuration::prefer_if(std::function<bool(VkPhysicalDevice, VkPhysicalDevice)> compare) {
	p->comparator = compare;
	return *this;
}


Device::Configuration& Device::Configuration::add_queues(const std::vector<float>& priorities, VkSurfaceKHR surface, VkQueueFlags flags) {
	p->priorities.push_back(priorities);
	p->surfaces.push_back(surface);
	p->flags.push_back(flags);
	return *this;
}


Device::Configuration& Device::Configuration::add_queues(const std::vector<float>& priorities, VkQueueFlags flags) {
	return add_queues(priorities, VK_NULL_HANDLE, flags);
}


Device::Configuration& Device::Configuration::add_queues(const std::vector<float>& priorities, VkSurfaceKHR surface) {
	return add_queues(priorities, surface, 0xffffffff);
}


Device Device::Configuration::create(VkInstance instance, VkPhysicalDevice device) {
	
	if (device == VK_NULL_HANDLE) {
		auto devices = get_physical_devices(instance);
		auto it = std::remove_if(devices.begin(), devices.end(), [this](VkPhysicalDevice device) -> bool {
			return !is_appropriate(device);
		});
		devices.erase(it, devices.end());

		auto best = std::min_element(devices.begin(), devices.end(), p->comparator);
		if (best == devices.end()) {
			return Device();
		} else {
			device = *best;
		}
	}

	p->device = device;


	std::map<uint64_t, std::set<uint64_t>> profile;

	auto family_properties = queue_family_properties(device);
	for (uint32_t i = 0; i < family_properties.size(); ++i) {
		profile[i].insert(i);
	}

	vkw::WaveFunctionCollapse wfc_algo(p->priorities.size());
	wfc_algo.add_profile(profile);

	for (size_t i = 0; i < p->priorities.size(); ++i) {
		for (size_t j = i + 1; j < p->priorities.size(); ++j) {
			wfc_algo.link_nodes(i, j, 0);
			wfc_algo.link_nodes(j, i, 0);
		}

		std::set<uint64_t> available_states;
		for (uint32_t j = 0; j < family_properties.size(); ++j) {
			if (p->surfaces[i] != VK_NULL_HANDLE) {
				VkBool32 supported;
				vkGetPhysicalDeviceSurfaceSupportKHR(device, j, p->surfaces[i], &supported);
				if (!supported) {
					continue;
				}
			}
			if (family_properties[j].queueCount < p->priorities[i].size()) {
				continue;
			}
			if (family_properties[j].queueFlags & p->flags[i]) {
				available_states.insert(j);
			}
		}
		wfc_algo.set_available_states(i, available_states);
	}

	std::chrono::system_clock clock;
	unsigned int seed = clock.now().time_since_epoch().count();
	std::default_random_engine rng(seed);
	auto family_indices = wfc_algo.evaluate();
	if (family_indices.size() != p->priorities.size()) {
		return Device();
	}
	 
	 
	for (size_t i = 0; i < p->flags.size(); ++i) {
		VkDeviceQueueCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		info.queueFamilyIndex = uint32_t(family_indices[i]);
		info.pNext = nullptr;
		info.pQueuePriorities = p->priorities[i].data();
		info.queueCount = uint32_t(p->priorities[i].size());
		p->queue_infos.push_back(info);
	}
	return Device(instance, *this);
}


std::vector<uint32_t> Device::Configuration::queue_family_indices() const {
	std::vector<uint32_t> result(p->queue_infos.size());
	auto func = [](VkDeviceQueueCreateInfo info) -> uint32_t {
		return info.queueFamilyIndex;
	};
	std::transform(p->queue_infos.begin(), p->queue_infos.end(), result.begin(), func);
	return result;
}


bool Device::Configuration::is_appropriate(VkPhysicalDevice device) const {
	auto available_extensions = vkw::get_available_extensions(device);
	for (auto it = p->required_extensions.begin(); it != p->required_extensions.end(); ++it) {
		auto it2 = std::find_if(
			available_extensions.begin(), 
			available_extensions.end(),
			[&](VkExtensionProperties prop) -> bool { return std::strcmp(*it, prop.extensionName) == 0; }
		);
		if (it2 == available_extensions.end()) {
			return false;
		}
	}
	return true;
}


}