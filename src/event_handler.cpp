#include "vkw/event_handler.hpp"

namespace vkw {

void EventHandler::on_key_press(Key, int, int) {

}

void EventHandler::on_key_release(Key, int, int) {

}

void EventHandler::on_mouse_button(MouseEvent, int, int) {

}

void EventHandler::on_mouse_move(int, int) {

}

void EventHandler::on_resize(uint32_t, uint32_t) {

}

void EventHandler::on_time_passed(const std::chrono::steady_clock::duration&) {

}

}