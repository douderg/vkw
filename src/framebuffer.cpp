#include "vkw/framebuffer.hpp"
#include <stdexcept>

namespace vkw {

Framebuffer::Framebuffer():device_{VK_NULL_HANDLE}, handle_{VK_NULL_HANDLE}, alloc_callbacks_{nullptr} {

}

Framebuffer::Framebuffer(
    VkDevice device, 
    const std::vector<VkImageView>& attachments, 
    VkExtent2D extent, 
    VkRenderPass render_pass, 
    size_t layers, 
    VkAllocationCallbacks* alloc_callbacks):
        device_{device}, 
        handle_{VK_NULL_HANDLE},
        alloc_callbacks_{alloc_callbacks} {

    VkFramebufferCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    info.attachmentCount = attachments.size();
    info.pAttachments = attachments.empty()? nullptr : attachments.data();
    info.renderPass = render_pass;
    info.width = extent.width;
    info.height = extent.height;
    info.layers = layers;

    if (vkCreateFramebuffer(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("Error: Failed to create framebuffer");
    }
}

Framebuffer::~Framebuffer() {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyFramebuffer(device_, handle_, alloc_callbacks_);
        handle_ = VK_NULL_HANDLE;
    }
}

Framebuffer::Framebuffer(Framebuffer&& other):device_{other.device_}, handle_{other.handle_}, alloc_callbacks_{other.alloc_callbacks_} {
    other.handle_ = VK_NULL_HANDLE;
}


Framebuffer& Framebuffer::operator=(Framebuffer&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyFramebuffer(device_, handle_, alloc_callbacks_);
    }
    device_ = other.device_;
    handle_ = other.handle_; 
    alloc_callbacks_ = other.alloc_callbacks_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

VkFramebuffer Framebuffer::handle() const {
    return handle_;
}

}