#include "vkw/image.hpp"
#include "vkw/allocator.hpp"
#include <stdexcept>

namespace vkw {

Image::Image():allocator_{VK_NULL_HANDLE}, handle_{VK_NULL_HANDLE} {

}

Image::Image(const Configuration& cfg, VmaAllocator allocator, VmaMemoryUsage mem_usage):
        Image(cfg, allocator, mem_usage, {}) {
    
}

Image::Image(const Configuration& cfg, VmaAllocator allocator, VmaMemoryUsage mem_usage, const std::vector<uint32_t>& queue_indices):
        allocator_{allocator},
        mip_levels_{cfg.mip_levels()},
        layers_{cfg.layers()},
        format_{cfg.format()},
        layout_{VK_IMAGE_LAYOUT_UNDEFINED},
        pipeline_stage_{VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT},
        access_flags_{} {
    
    VkImageCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    info.usage = cfg.usage();
    info.imageType = cfg.type();
    info.extent = cfg.extent();
    info.format = format_;
    info.initialLayout = layout_;
    info.tiling = cfg.tiling();
    info.mipLevels = mip_levels_;
    info.samples = cfg.samples();
    info.arrayLayers = layers_;
    info.sharingMode = queue_indices.empty()? VK_SHARING_MODE_EXCLUSIVE : VK_SHARING_MODE_CONCURRENT;
    info.queueFamilyIndexCount = static_cast<uint32_t>(queue_indices.size());
    info.pQueueFamilyIndices = queue_indices.empty()? nullptr : queue_indices.data();
    
    VmaAllocationCreateInfo alloc_info = {};
    alloc_info.usage = mem_usage;
    if (vmaCreateImage(allocator_, &info, &alloc_info, &handle_, &allocation_, nullptr) != VK_SUCCESS) {
        throw std::runtime_error("failed to create image");
    }
}

Image::~Image() {
    if (handle_ != VK_NULL_HANDLE) {
        vmaDestroyImage(allocator_, handle_, allocation_);
        handle_ = VK_NULL_HANDLE;
    }
}

Image::Image(Image&& other):
        allocator_{other.allocator_}, 
        handle_{other.handle_}, 
        allocation_{other.allocation_},
        format_{other.format_},
        access_flags_{other.access_flags_}
{
    other.handle_ = VK_NULL_HANDLE;
}

Image& Image::operator=(Image&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vmaDestroyImage(allocator_, handle_, allocation_);
    }
    allocator_ = other.allocator_; 
    handle_ = other.handle_; 
    allocation_ = other.allocation_;
    format_ = other.format_;
    access_flags_ = other.access_flags_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

VkImage Image::handle() const {
    return handle_;
}

ImageView Image::create_view(VkImageAspectFlags aspect) const {
    if (allocator_ != VK_NULL_HANDLE && handle_ != VK_NULL_HANDLE) {
        return ImageView{get_allocator_device(allocator_), handle_, VK_IMAGE_VIEW_TYPE_2D, format_, aspect};
    }
    throw std::runtime_error("attempted to create view from an uninitialized image");
}

Image& Image::change_layout(VkCommandBuffer buffer, VkImageLayout layout, VkPipelineStageFlags stage, VkAccessFlags access_mask, VkImageAspectFlags aspect) {
    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.image = handle_;
    barrier.oldLayout = layout_;
    barrier.newLayout = layout;
    barrier.srcAccessMask = access_flags_;
    barrier.dstAccessMask = access_mask;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.subresourceRange.aspectMask = aspect;
    barrier.subresourceRange.levelCount = mip_levels_;
    barrier.subresourceRange.layerCount = layers_;
    for (uint32_t i = 0; i < layers_; ++i) {
        barrier.subresourceRange.baseArrayLayer = i;
        for (uint32_t j = 0; j < mip_levels_; ++j) {
            barrier.subresourceRange.baseMipLevel = j;
            vkCmdPipelineBarrier(buffer, pipeline_stage_, stage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
        }
    }
    layout_ = layout;
    pipeline_stage_ = stage;
    access_flags_ = access_mask;
    return *this;
}


Image::Factory::Factory()
{
    type_ = VK_IMAGE_TYPE_2D;
    levels_ = 1;
    layers_ = 1;
    format_ = VK_FORMAT_R8G8B8A8_SRGB;
    tiling_ = VK_IMAGE_TILING_OPTIMAL;
    usage_ = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    samples_ = VK_SAMPLE_COUNT_1_BIT;
}


VkImageType Image::Factory::type() const {
    return type_;
}


VkFormat Image::Factory::format() const {
    return format_;
}


VkExtent3D Image::Factory::extent() const {
    return extent_;
}


uint32_t Image::Factory::mip_levels() const {
    return levels_;
}


uint32_t Image::Factory::layers() const {
    return layers_;
}


VkSampleCountFlagBits Image::Factory::samples() const {
    return samples_;
}


VkImageTiling Image::Factory::tiling() const {
    return tiling_;
}


VkImageUsageFlags Image::Factory::usage() const {
    return usage_;
}


Image::Factory& Image::Factory::extent(uint32_t width, uint32_t height, uint32_t depth) {
    extent_.width = width;
    extent_.height = height;
    extent_.depth = depth;
    return *this;
}


Image::Factory& Image::Factory::extent(const VkExtent3D& extent) {
    extent_ = extent;
    return *this;
}


Image::Factory& Image::Factory::format(VkFormat fmt) {
    format_ = fmt;
    return *this;
}


Image::Factory& Image::Factory::usage(VkImageUsageFlags flags) {
    usage_ = flags;
    return *this;
}


Image Image::Factory::create(VmaAllocator allocator, VmaMemoryUsage mem_usage) const {
    return Image(*this, allocator, mem_usage);
}


}