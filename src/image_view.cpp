#include "vkw/image_view.hpp"
#include <stdexcept>

namespace vkw {

ImageView::ImageView():handle_{VK_NULL_HANDLE} {

}

ImageView::ImageView(VkDevice device, VkImage image, VkFormat format, VkAllocationCallbacks* alloc_callbacks):
        ImageView{device, image, VK_IMAGE_VIEW_TYPE_2D, format, VK_IMAGE_ASPECT_COLOR_BIT, alloc_callbacks} {

    
}

ImageView::ImageView(VkDevice device, VkImage image, VkImageViewType type, VkFormat format, VkImageAspectFlags aspect, VkAllocationCallbacks* alloc_callbacks):
        device_{device}, 
        handle_{VK_NULL_HANDLE}, 
        alloc_callbacks_{alloc_callbacks} {
    VkImageViewCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    info.image = image;
    info.format = format;
    info.viewType = type;
    info.subresourceRange.aspectMask = aspect;
    info.subresourceRange.baseMipLevel = 0;
    info.subresourceRange.levelCount = 1;
    info.subresourceRange.baseArrayLayer = 0;
    info.subresourceRange.layerCount = 1;

    if (vkCreateImageView(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("Error: Failed to create image view");
    }
}

ImageView::~ImageView() {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyImageView(device_, handle_, alloc_callbacks_);
        handle_ = VK_NULL_HANDLE;
    }
}

ImageView::ImageView(ImageView&& other):
    device_{other.device_}, 
    handle_{other.handle_}, 
    alloc_callbacks_{other.alloc_callbacks_} 
{
    other.handle_ = VK_NULL_HANDLE;
}

ImageView& ImageView::operator=(ImageView&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyImageView(device_, handle_, alloc_callbacks_);
    }
    device_ = other.device_;
    handle_ = other.handle_;
    alloc_callbacks_ = other.alloc_callbacks_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

VkImageView ImageView::handle() const {
    return handle_;
}

}