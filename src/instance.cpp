#include <vkw/instance.hpp>
#include <vkw/surface.hpp>

#ifdef USE_SDL
#include <SDL_vulkan.h>
#endif

namespace vkw {

std::vector<VkExtensionProperties> get_available_extensions() {
	uint32_t count = 0;
	std::vector<VkExtensionProperties> result;
	if (vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get supported instance extensions");
	}
	if (!count) {
		return result;
	}
	result.resize(count);
	if (vkEnumerateInstanceExtensionProperties(nullptr, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get supported instance extensions");
	}
	return result;
}

Instance::Instance(VkAllocationCallbacks* callbacks): 
	handle_{VK_NULL_HANDLE},
	alloc_callbacks_{callbacks} 
{
}

Instance::Instance(const Configuration& cfg, VkAllocationCallbacks* callbacks):
		handle_{VK_NULL_HANDLE},
		alloc_callbacks_{callbacks} {
	
	auto available_extensions = get_available_extensions();
	auto required_extensions = cfg.extensions();
	for (auto it = required_extensions.begin(); it != required_extensions.end(); ++it) {
		auto it2 = std::find_if(
			available_extensions.begin(), 
			available_extensions.end(), 
			[&](VkExtensionProperties prop) -> bool { return std::strcmp(*it, prop.extensionName) == 0; }
		);
		if (it2 == available_extensions.end()) {
			throw std::runtime_error("Error: Required extension '" + std::string(*it) + "' is not supported");
		}
	}

	auto enabled_layers = cfg.layers();
	VkInstanceCreateInfo info{};
	info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	info.enabledExtensionCount = required_extensions.size();
	info.ppEnabledExtensionNames = required_extensions.empty()? nullptr : required_extensions.data();
	info.enabledLayerCount = enabled_layers.size();
	info.ppEnabledLayerNames = enabled_layers.empty()? nullptr : enabled_layers.data();

	if (vkCreateInstance(&info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create Vulkan instance");
	}
}

Instance::Instance(Instance&& other):
		handle_{std::move(other.handle_)},
		alloc_callbacks_{std::move(other.alloc_callbacks_)} {
	other.handle_ = VK_NULL_HANDLE;
}

Instance::~Instance() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyInstance(handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}

Instance& Instance::operator=(Instance&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyInstance(handle_, alloc_callbacks_);
	}
	handle_ = std::move(other.handle_);
	alloc_callbacks_ = std::move(other.alloc_callbacks_);
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}

VkInstance Instance::handle() const {
	return handle_;
}

const std::vector<const char*>& Instance::Configuration::extensions() const {
	return extensions_;
}

const std::vector<const char*>& Instance::Configuration::layers() const {
	return layers_;
}

Instance::Configuration& Instance::Configuration::extensions(const std::vector<const char*>& value) {
	extensions_ = value;
	return *this;
}

Instance::Configuration& Instance::Configuration::layers(const std::vector<const char*>& value) {
	layers_ = value;
	return *this;
}

}