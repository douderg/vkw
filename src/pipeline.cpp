#include <vkw/pipeline.hpp>

namespace vkw {

Viewport::Viewport(float width, float height): value_{} {
	value_.width = width;
	value_.height = height;
}


Viewport::Viewport(VkExtent2D extent): value_{} {
	value_.width = (float) extent.width;
	value_.height = (float) extent.height;
}


Viewport& Viewport::offset(VkOffset2D offset) {
	value_.x = (float) offset.x;
	value_.y = (float) offset.y;
	return *this;
}


Viewport& Viewport::offset(float x, float y) {
	value_.x = x;
	value_.y = y;
	return *this;
}


Viewport& Viewport::extent(VkExtent2D extent) {
	value_.width = (float) extent.width;
	value_.height = (float) extent.height;
	return *this;
}


Viewport& Viewport::extent(float width, float height) {
	value_.width = width;
	value_.height = height;
	return *this;
}


Viewport& Viewport::depth(float min, float max) {
	value_.minDepth = min;
	value_.maxDepth = max;
	return *this;
}


VkViewport Viewport::handle() const {
	return value_;
}


Scissor::Scissor(VkExtent2D extent): value_{} {
	value_.extent = extent;
}


Scissor::Scissor(VkOffset2D offset, VkExtent2D extent): value_{} {
	value_.offset = offset;
	value_.extent = extent;
}


VkRect2D Scissor::handle() const {
	return value_;
}


Pipeline::Layout::Layout():
		handle_{VK_NULL_HANDLE},
		device_{VK_NULL_HANDLE},
		alloc_callbacks_{nullptr} {
	
}


Pipeline::Layout::Layout(VkDevice device, const Configuration& cfg, VkAllocationCallbacks* callbacks):
		handle_{VK_NULL_HANDLE},
		device_{device},
		alloc_callbacks_{callbacks} {
	VkPipelineLayoutCreateInfo info = cfg.create_info();
	if (vkCreatePipelineLayout(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("Error: failed to create pipeline layout");
	}
}


Pipeline::Layout::Layout(Layout&& other):
		handle_{std::move(other.handle_)},
		device_{std::move(other.device_)},
		alloc_callbacks_{std::move(other.alloc_callbacks_)} {
	other.handle_ = VK_NULL_HANDLE;
}


Pipeline::Layout::~Layout() {
	if (handle_ !=  VK_NULL_HANDLE) {
		vkDestroyPipelineLayout(device_, handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}


Pipeline::Layout& Pipeline::Layout::operator=(Layout&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyPipelineLayout(device_, handle_, alloc_callbacks_);
	}
	handle_ = std::move(other.handle_);
	device_ = std::move(other.device_);
	alloc_callbacks_ = std::move(other.alloc_callbacks_);
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}


VkPipelineLayout Pipeline::Layout::handle() const {
	return handle_;
}


Pipeline::Layout::Configuration& Pipeline::Layout::Configuration::add(VkDescriptorSetLayout layout) {
	set_layouts_.push_back(layout);
	return *this;
}


VkPipelineLayoutCreateInfo Pipeline::Layout::Configuration::create_info() const {
	VkPipelineLayoutCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	info.setLayoutCount = uint32_t(set_layouts_.size());
	info.pSetLayouts = set_layouts_.empty()? nullptr : set_layouts_.data();
	info.pushConstantRangeCount = uint32_t(push_constant_ranges_.size());
	info.pPushConstantRanges = push_constant_ranges_.empty()? nullptr : push_constant_ranges_.data();
	return info;
}


struct Pipeline::VertexInputState::Pimpl {
	Pimpl();
	std::vector<VkVertexInputBindingDescription> bindings;
	std::vector<VkVertexInputAttributeDescription> attributes;
	VkPipelineVertexInputStateCreateInfo info;
};


Pipeline::VertexInputState::Pimpl::Pimpl():
	info{}
{
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
}


Pipeline::VertexInputState::VertexInputState(): 
	p{nullptr}
{
	p = new Pimpl();
}


Pipeline::VertexInputState::~VertexInputState() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Pipeline::VertexInputState::VertexInputState(const VertexInputState& other): 
	p{nullptr}
{
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pVertexAttributeDescriptions = p->attributes.empty()? nullptr : p->attributes.data();
		p->info.pVertexBindingDescriptions = p->bindings.empty()? nullptr : p->bindings.data();
	}
}


Pipeline::VertexInputState::VertexInputState(VertexInputState&& other):
	p{other.p}
{
	other.p = nullptr;
}


Pipeline::VertexInputState& Pipeline::VertexInputState::operator=(const VertexInputState& other) {
	if (p) {
		delete p;
		p = nullptr;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pVertexAttributeDescriptions = p->attributes.empty()? nullptr : p->attributes.data();
		p->info.pVertexBindingDescriptions = p->bindings.empty()? nullptr : p->bindings.data();
	}
	return *this;
}


Pipeline::VertexInputState& Pipeline::VertexInputState::operator=(VertexInputState&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


Pipeline::VertexInputState& Pipeline::VertexInputState::create_binding(uint32_t id, VkVertexInputRate rate, uint32_t stride) {
	if (!p) {
		throw std::runtime_error("invalid vertex input state");
	}
	VkVertexInputBindingDescription binding;
	binding.binding = id;
	binding.inputRate = rate;
	binding.stride = stride;
	p->bindings.push_back(binding);
	p->info.vertexBindingDescriptionCount = uint32_t(p->bindings.size());
	p->info.pVertexBindingDescriptions = p->bindings.data();
	return *this;
}


Pipeline::VertexInputState& Pipeline::VertexInputState::bind_attribute(uint32_t binding, uint32_t location, const VkFormat &format, uint32_t offset) {
	if (!p) {
		throw std::runtime_error("invalid vertex input state");
	}
	VkVertexInputAttributeDescription attribute;
	attribute.binding = binding;
	attribute.location = location;
	attribute.format = format;
	attribute.offset = offset;
	p->attributes.push_back(attribute);
	p->info.vertexAttributeDescriptionCount = uint32_t(p->attributes.size());
	p->info.pVertexAttributeDescriptions = p->attributes.data();
	return *this;
}


const VkPipelineVertexInputStateCreateInfo* Pipeline::VertexInputState::info() const {
	if (!p) {
		throw std::runtime_error("invalid vertex input state");
	}
	return &p->info;
}


Pipeline::InputAssemblyState::InputAssemblyState(): info_{} {
	info_.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	info_.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	info_.primitiveRestartEnable = VK_FALSE;
}


Pipeline::InputAssemblyState& Pipeline::InputAssemblyState::topology(VkPrimitiveTopology value) {
	info_.topology = value;
	return *this;
}


Pipeline::InputAssemblyState& Pipeline::InputAssemblyState::primitive_restart(VkBool32 value) {
	info_.primitiveRestartEnable = value;
	return *this;
}


const VkPipelineInputAssemblyStateCreateInfo* Pipeline::InputAssemblyState::info() const {
	return &info_;
}


struct Pipeline::ViewportState::Pimpl {
	Pimpl();
	std::vector<VkViewport> viewports;
	std::vector<VkRect2D> scissors;
	VkPipelineViewportStateCreateInfo info;
};


Pipeline::ViewportState::Pimpl::Pimpl():
	info{}
{
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
}


Pipeline::ViewportState::ViewportState():
	p{nullptr}
{
	p = new Pimpl();
}


Pipeline::ViewportState::~ViewportState() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Pipeline::ViewportState::ViewportState(const ViewportState& other): 
	p{nullptr}
{
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pViewports = p->viewports.empty()? nullptr : p->viewports.data();
		p->info.pScissors = p->scissors.empty()? nullptr : p->scissors.data();
	}
}


Pipeline::ViewportState::ViewportState(ViewportState&& other): 
	p{other.p}  
{
	other.p = nullptr;
}


Pipeline::ViewportState& Pipeline::ViewportState::operator=(const ViewportState& other) {
	if (p) {
		delete p;
		p = nullptr;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pViewports = p->viewports.empty()? nullptr : p->viewports.data();
		p->info.pScissors = p->scissors.empty()? nullptr : p->scissors.data();
	}
	return *this;
}


Pipeline::ViewportState& Pipeline::ViewportState::operator=(ViewportState&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


Pipeline::ViewportState& Pipeline::ViewportState::add(const Viewport& viewport) {
	if (!p) {
		throw std::runtime_error("invalid viewport state");
	}
	p->viewports.push_back(viewport.handle());
	p->info.viewportCount = uint32_t(p->viewports.size());
	p->info.pViewports = p->viewports.data();
	return *this;
}


Pipeline::ViewportState& Pipeline::ViewportState::add(const Scissor& scissor) {
	if (!p) {
		throw std::runtime_error("invalid viewport state");
	}
	p->scissors.push_back(scissor.handle());
	p->info.scissorCount = uint32_t(p->scissors.size());
	p->info.pScissors = p->scissors.data();
	return *this;
}


const VkPipelineViewportStateCreateInfo* Pipeline::ViewportState::info() const {
	if (!p) {
		throw std::runtime_error("invalid viewport state");
	}
	return &p->info;
}


Pipeline::RasterizationState::RasterizationState(): info_{} {
	info_.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	info_.depthClampEnable = VK_FALSE;
	info_.rasterizerDiscardEnable = VK_FALSE;
	info_.polygonMode = VK_POLYGON_MODE_FILL;
	info_.lineWidth = 1.0f;
	info_.cullMode = VK_CULL_MODE_BACK_BIT;
	info_.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	info_.depthBiasEnable = VK_FALSE;
	info_.depthBiasConstantFactor = 0.0f;
	info_.depthBiasClamp = 0.0f;
	info_.depthBiasSlopeFactor = 0.0f;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::polygon_mode(VkPolygonMode value) {
	info_.polygonMode = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::front_face(VkFrontFace value) {
	info_.frontFace = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::cull_mode(VkCullModeFlags value) {
	info_.cullMode = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::depth_bias_enabled(VkBool32 value) {
	info_.depthBiasEnable = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::depth_bias_constant_factor(float value) {
	info_.depthBiasConstantFactor = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::depth_bias_clamp(float value) {
	info_.depthBiasClamp = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::depth_bias_slope_factor(float value) {
	info_.depthBiasSlopeFactor = value;
	return *this;
}


Pipeline::RasterizationState& Pipeline::RasterizationState::line_width(float value) {
	info_.lineWidth = value;
	return *this;
}


const VkPipelineRasterizationStateCreateInfo* Pipeline::RasterizationState::info() const {
	return &info_;
}


struct Pipeline::MultisampleState::Pimpl {
	Pimpl();
	VkPipelineMultisampleStateCreateInfo info;
	std::vector<VkSampleMask> sample_masks;
};


Pipeline::MultisampleState::Pimpl::Pimpl():
	info{}
{
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	info.sampleShadingEnable = VK_FALSE;
	info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	info.minSampleShading = 1.0f;
	info.pSampleMask = nullptr;
	info.alphaToCoverageEnable = VK_FALSE;
	info.alphaToOneEnable = VK_FALSE;
}


Pipeline::MultisampleState::MultisampleState():
	p{nullptr}
{
	p = new Pimpl();
}


Pipeline::MultisampleState::~MultisampleState() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Pipeline::MultisampleState::MultisampleState(const MultisampleState& other):
	p{nullptr}
{
	p = new Pimpl(*other.p);
	p->info.pSampleMask = p->sample_masks.empty()? nullptr : p->sample_masks.data();
}


Pipeline::MultisampleState::MultisampleState(MultisampleState&& other):
	p{other.p}
{
	other.p = nullptr;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::operator=(const MultisampleState& other) {
	if (p) {
		delete p;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pSampleMask = p->sample_masks.empty()? nullptr : p->sample_masks.data();
	}
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::operator=(MultisampleState&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::samples_per_pixel(VkSampleCountFlagBits value) {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	p->info.rasterizationSamples = value;
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::sample_shading_enable(VkBool32 value) {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	p->info.sampleShadingEnable = value;
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::min_sample_shading(float value) {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	p->info.minSampleShading = value;
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::sample_masks(const std::vector<VkSampleMask>& value) {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	p->sample_masks = value;
	p->info.pSampleMask = p->sample_masks.data();
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::alpha_to_coverage_enable(VkBool32 value) {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	p->info.alphaToCoverageEnable = value;
	return *this;
}


Pipeline::MultisampleState& Pipeline::MultisampleState::alpha_to_one_enable(VkBool32 value) {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	p->info.alphaToOneEnable = value;
	return *this;
}


const VkPipelineMultisampleStateCreateInfo* Pipeline::MultisampleState::info() const {
	if (!p) {
		throw std::runtime_error("invalid multisample state");
	}
	return &p->info;
}


Pipeline::DepthStencilState::DepthStencilState(): info_{} {
	info_.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::depth_test(VkBool32 value, VkCompareOp comp) {
	info_.depthTestEnable = value;
	info_.depthCompareOp = comp;
	return *this;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::depth_write(VkBool32 value) {
	info_.depthWriteEnable = value;
	return *this;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::stencil_test(VkBool32 value) {
	info_.stencilTestEnable = value;
	return *this;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::depth_bounds_test(VkBool32 value) {
	info_.depthBoundsTestEnable = value;
	return *this;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::depth_bounds(float min, float max) {
	info_.minDepthBounds = min;
	info_.maxDepthBounds = max;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState Pipeline::DepthStencilState::stencil_op() {
	return StencilOpState(*this);
}


Pipeline::DepthStencilState::StencilOpState::StencilOpState(Pipeline::DepthStencilState& state):
	state_(state),
	op_state_{}
{
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::on_fail(VkStencilOp value) {
	op_state_.failOp = value;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::on_pass(VkStencilOp value) {
	op_state_.passOp = value;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::on_depth_fail(VkStencilOp value) {
	op_state_.depthFailOp = value;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::compare_op(VkCompareOp value) {
	op_state_.compareOp = value;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::compare_mask(uint32_t value) {
	op_state_.compareMask = value;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::write_mask(uint32_t value) {
	op_state_.writeMask = value;
	return *this;
}


Pipeline::DepthStencilState::StencilOpState& Pipeline::DepthStencilState::StencilOpState::reference_mask(uint32_t value) {
	op_state_.reference = value;
	return *this;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::StencilOpState::front_test() {
	state_.info_.front = op_state_;
	return state_;
}


Pipeline::DepthStencilState& Pipeline::DepthStencilState::StencilOpState::back_test() {
	state_.info_.back = op_state_;
	return state_;
}


const VkPipelineDepthStencilStateCreateInfo* Pipeline::DepthStencilState::info() const {
	return &info_;
}


Pipeline::ColorBlendState::Attachment::Attachment(): state_{} {
	state_.blendEnable = VK_FALSE;
	state_.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	state_.srcColorBlendFactor = state_.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	state_.dstColorBlendFactor = state_.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	state_.colorBlendOp = state_.alphaBlendOp = VK_BLEND_OP_ADD;
}


Pipeline::ColorBlendState::Attachment& Pipeline::ColorBlendState::Attachment::blend(VkBool32 enabled) {
	state_.blendEnable = enabled;
	return *this;
}


Pipeline::ColorBlendState::Attachment& Pipeline::ColorBlendState::Attachment::write_mask(VkColorComponentFlags mask) {
	state_.colorWriteMask = mask;
	return *this;
}


Pipeline::ColorBlendState::Attachment& Pipeline::ColorBlendState::Attachment::blend_color(VkBlendOp op, VkBlendFactor src, VkBlendFactor dest) {
	state_.colorBlendOp = op;
	state_.srcColorBlendFactor = src;
	state_.dstColorBlendFactor = dest;
	return *this;
}


Pipeline::ColorBlendState::Attachment& Pipeline::ColorBlendState::Attachment::blend_alpha(VkBlendOp op, VkBlendFactor src, VkBlendFactor dest) {
	state_.alphaBlendOp = op;
	state_.srcAlphaBlendFactor = src;
	state_.dstAlphaBlendFactor = dest;
	return *this;
}


VkPipelineColorBlendAttachmentState Pipeline::ColorBlendState::Attachment::state() const {
	return state_;
}


struct Pipeline::ColorBlendState::Pimpl {
	Pimpl();
	std::vector<VkPipelineColorBlendAttachmentState> attachments;
	VkPipelineColorBlendStateCreateInfo info;
};


Pipeline::ColorBlendState::Pimpl::Pimpl():
	info{}
{
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	info.attachmentCount = 0;
	info.pAttachments = nullptr;
	info.logicOpEnable = VK_FALSE;
	std::memset(info.blendConstants, 0, 4 * sizeof(float));
}


Pipeline::ColorBlendState::ColorBlendState():
	p{nullptr} 
{
	p = new Pimpl();
}


Pipeline::ColorBlendState::~ColorBlendState() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Pipeline::ColorBlendState::ColorBlendState(const ColorBlendState& other):
	p{nullptr}
{
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pAttachments = p->attachments.empty()? nullptr : p->attachments.data();
		std::memcpy(p->info.blendConstants, other.p->info.blendConstants, 4 * sizeof(float));
	}
}


Pipeline::ColorBlendState::ColorBlendState(ColorBlendState&& other):
	p{other.p}
{
	other.p = nullptr;
}


Pipeline::ColorBlendState& Pipeline::ColorBlendState::operator=(const ColorBlendState& other) {
	if (p) {
		delete p;
		p = nullptr;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pAttachments = p->attachments.empty()? nullptr : p->attachments.data();
		std::memcpy(p->info.blendConstants, other.p->info.blendConstants, 4 * sizeof(float));
	}
	return *this;
}


Pipeline::ColorBlendState& Pipeline::ColorBlendState::operator=(ColorBlendState&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


Pipeline::ColorBlendState& Pipeline::ColorBlendState::add(const Attachment& attachment) {
	if (!p) {
		throw std::runtime_error("invalid color blend state");
	}
	p->attachments.push_back(attachment.state());
	p->info.attachmentCount = uint32_t(p->attachments.size());
	p->info.pAttachments = p->attachments.data();
	return *this;
}


Pipeline::ColorBlendState& Pipeline::ColorBlendState::logic_op(VkLogicOp op) {
	if (!p) {
		throw std::runtime_error("invalid color blend state");
	}
	p->info.logicOpEnable = VK_TRUE;
	p->info.logicOp = op;
	return *this;
}


Pipeline::ColorBlendState& Pipeline::ColorBlendState::blend_constants(float r, float g, float b, float a) {
	if (!p) {
		throw std::runtime_error("invalid color blend state");
	}
	std::array<float, 4> blend_constants = {r, g, b, a};
	std::memcpy(p->info.blendConstants, blend_constants.data(), blend_constants.size() * sizeof(float));
	return *this;
}


const VkPipelineColorBlendStateCreateInfo* Pipeline::ColorBlendState::info() const {
	if (!p) {
		throw std::runtime_error("invalid color blend state");
	}
	return &p->info;
}


struct Pipeline::DynamicState::Pimpl {
	Pimpl();
	std::vector<VkDynamicState> states;
	VkPipelineDynamicStateCreateInfo info;
};


Pipeline::DynamicState::Pimpl::Pimpl():
	info{}
{
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
}


Pipeline::DynamicState::DynamicState():
	p{nullptr}
{
	p = new Pimpl();
}


Pipeline::DynamicState::DynamicState(const std::vector<VkDynamicState> &states):
	p{nullptr}
{
	p = new Pimpl();
	p->states = states;
	p->info.dynamicStateCount = uint32_t(p->states.size());
	p->info.pDynamicStates = p->states.empty() ? nullptr : p->states.data();
}


Pipeline::DynamicState::~DynamicState() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Pipeline::DynamicState::DynamicState(const DynamicState& other): 
	p{nullptr}
{
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pDynamicStates = p->states.empty() ? nullptr : p->states.data();
	}
}


Pipeline::DynamicState::DynamicState(DynamicState&& other):
	p{other.p}
{
	other.p = nullptr;
}


Pipeline::DynamicState& Pipeline::DynamicState::operator=(const DynamicState& other) {
	if (p) {
		delete p;
		p = nullptr;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
		p->info.pDynamicStates = p->states.empty() ? nullptr : p->states.data();
	}
	return *this;
}


Pipeline::DynamicState& Pipeline::DynamicState::operator=(DynamicState&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


const VkPipelineDynamicStateCreateInfo* Pipeline::DynamicState::info() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline dynamic state");
	}
	return &p->info;
}


struct Pipeline::Configuration::Pimpl {
	Pimpl();

	std::vector<VkPipelineShaderStageCreateInfo> shaders;
	VertexInputState vertex_input_state;
	InputAssemblyState input_assembly_state;
	ViewportState viewport_state;
	RasterizationState rasterization_state;
	MultisampleState multisample_state;
	DepthStencilState depth_stencil_state;
	ColorBlendState color_blend_state;
	DynamicState dynamic_state;

	VkPipelineCreateFlags flags;
	VkPipelineLayout layout;
	VkRenderPass render_pass;
	uint32_t subpass;
	VkPipeline base_pipeline_handle;
};


Pipeline::Configuration::Pimpl::Pimpl():
	flags{0},
	subpass{0}
{
}


Pipeline::Configuration::Configuration():
	p{nullptr}
{
	p = new Pimpl();
}


Pipeline::Configuration::~Configuration() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Pipeline::Configuration::Configuration(const Configuration& other):
	p{nullptr}
{
	if (other.p) {
		p = new Pimpl(*other.p);
	}
}


Pipeline::Configuration::Configuration(Configuration&& other):
	p{other.p}
{
	other.p = nullptr;
}


Pipeline::Configuration& Pipeline::Configuration::operator=(const Configuration& other) {
	if (p) {
		delete p;
		p = nullptr;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
	}
	return *this;
}


Pipeline::Configuration& Pipeline::Configuration::operator=(Configuration&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


Pipeline::Configuration& Pipeline::Configuration::add_shader(VkShaderModule shader, VkShaderStageFlagBits stage) {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	VkPipelineShaderStageCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	info.module = shader;
	info.stage = stage;
	info.pName = "main";
	p->shaders.push_back(info);
	return *this;
}


VkGraphicsPipelineCreateInfo Pipeline::Configuration::create_info() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	VkGraphicsPipelineCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	info.stageCount = uint32_t(p->shaders.size());
	info.pStages = p->shaders.empty()? nullptr : p->shaders.data();
	info.pVertexInputState = p->vertex_input_state.info();
	info.pInputAssemblyState = p->input_assembly_state.info();
	info.pViewportState = p->viewport_state.info();
	info.pRasterizationState = p->rasterization_state.info();
	info.pMultisampleState = p->multisample_state.info();
	info.pDepthStencilState = p->depth_stencil_state.info();
	info.pColorBlendState = p->color_blend_state.info();
	info.pDynamicState = p->dynamic_state.info();

	info.flags = p->flags;
	info.layout = p->layout;
	info.renderPass = p->render_pass;
	info.subpass = p->subpass;
	info.basePipelineHandle = VK_NULL_HANDLE; //base_pipeline_handle;
	info.basePipelineIndex = -1; // todo
	return info;
}


Pipeline::VertexInputState& Pipeline::Configuration::vertex_input_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->vertex_input_state;
}


Pipeline::InputAssemblyState& Pipeline::Configuration::input_assembly_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->input_assembly_state;
}


Pipeline::ViewportState& Pipeline::Configuration::viewport_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->viewport_state;
}


Pipeline::RasterizationState& Pipeline::Configuration::rasterization_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->rasterization_state;
}


Pipeline::MultisampleState& Pipeline::Configuration::multisample_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->multisample_state;
}


Pipeline::DepthStencilState& Pipeline::Configuration::depth_stencil_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->depth_stencil_state;
}


Pipeline::ColorBlendState& Pipeline::Configuration::color_blend_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->color_blend_state;
}


Pipeline::DynamicState& Pipeline::Configuration::dynamic_state() {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->dynamic_state;
}


VkPipelineCreateFlags Pipeline::Configuration::flags() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->flags;
}


VkPipelineLayout Pipeline::Configuration::layout() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->layout;
}


VkRenderPass Pipeline::Configuration::render_pass() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->render_pass;
}


uint32_t Pipeline::Configuration::subpass() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	return p->subpass;
}


Pipeline::Configuration& Pipeline::Configuration::flags(VkPipelineCreateFlags value) {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	p->flags = value;
	return *this;
}


Pipeline::Configuration& Pipeline::Configuration::layout(VkPipelineLayout value) {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	p->layout = value;
	return *this;
}


Pipeline::Configuration& Pipeline::Configuration::render_pass(VkRenderPass value) {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	p->render_pass = value;
	return *this;
}


Pipeline::Configuration& Pipeline::Configuration::subpass(uint32_t value) {
	if (!p) {
		throw std::runtime_error("invalid pipeline configuration");
	}
	p->subpass = value;
	return *this;
}


Pipeline::Pipeline():device_{VK_NULL_HANDLE}, handle_{VK_NULL_HANDLE}, alloc_callbacks_{nullptr} {

}


Pipeline::Pipeline(VkPipeline handle, VkDevice device, VkAllocationCallbacks* callbacks):
		device_{device}, handle_{handle}, alloc_callbacks_{callbacks} {

}


Pipeline::Pipeline(Pipeline&& other):
	device_{other.device_}, 
	handle_{other.handle_}, 
	alloc_callbacks_{other.alloc_callbacks_} 
{
	other.handle_ = VK_NULL_HANDLE;
}


Pipeline::~Pipeline() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyPipeline(device_, handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}


Pipeline& Pipeline::operator=(Pipeline&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyPipeline(device_, handle_, alloc_callbacks_);
	}
	device_ = other.device_;
	handle_ = other.handle_;
	alloc_callbacks_ = other.alloc_callbacks_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}


VkPipeline Pipeline::handle() const {
	return handle_;
}


struct GraphicsPipelines::Pimpl {
	std::unordered_map<std::string, Pipeline> pipelines;
};


GraphicsPipelines::GraphicsPipelines():
	p{nullptr}
{
	p = new Pimpl();
}

GraphicsPipelines::~GraphicsPipelines() {
	if (p) {
		delete p;
		p = nullptr;
	}
}

GraphicsPipelines::GraphicsPipelines(const Factory& factory, VkDevice device, VkAllocationCallbacks* callbacks):
	p{nullptr}
{
	p = new Pimpl();
	auto pipelines = factory.pipelines();
	std::vector<VkGraphicsPipelineCreateInfo> create_infos;
	for (const auto& pipeline : pipelines) {
		create_infos.push_back(factory[pipeline].create_info());
	}
	std::vector<VkPipeline> handles(create_infos.size());
	if (vkCreateGraphicsPipelines(device, factory.cache(), uint32_t(create_infos.size()), create_infos.data(), callbacks, handles.data()) != VK_SUCCESS) {
		throw std::runtime_error("error: failed to create graphics pipeline");
	}
	for (size_t i = 0; i < handles.size(); ++i) {
		p->pipelines.emplace(pipelines[i], std::move(Pipeline(handles[i], device, callbacks)));
	}
}


GraphicsPipelines::GraphicsPipelines(GraphicsPipelines&& other):
	p{other.p}
{
	other.p = nullptr;
}


GraphicsPipelines& GraphicsPipelines::operator=(GraphicsPipelines&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


const Pipeline& GraphicsPipelines::operator[](const std::string& key) const {
	if (!p) {
		throw std::runtime_error("invalid graphics pipelines");
	}
	return p->pipelines.at(key);
}


struct GraphicsPipelines::Factory::Pimpl {
	Pimpl();
	std::unordered_map<std::string, Pipeline::Configuration> pipelines;
	VkPipelineCache cache;
};


GraphicsPipelines::Factory::Pimpl::Pimpl():
	cache{VK_NULL_HANDLE}
{
}


GraphicsPipelines::Factory::Factory():
	p{nullptr}
{
	p = new Pimpl();
}


GraphicsPipelines::Factory::~Factory() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


GraphicsPipelines::Factory::Factory(const Factory& other):
	p{nullptr}
{
	if (other.p) {
		p = new Pimpl(*other.p);
	}
}


GraphicsPipelines::Factory::Factory(Factory&& other):
	p{other.p}
{
	other.p = nullptr;
}


GraphicsPipelines::Factory& GraphicsPipelines::Factory::operator=(const Factory& other) {
	if (p) {
		delete p;
		p = nullptr;
	}
	if (other.p) {
		p = new Pimpl(*other.p);
	}
	return *this;
}


GraphicsPipelines::Factory& GraphicsPipelines::Factory::operator=(Factory&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


GraphicsPipelines::Factory& GraphicsPipelines::Factory::add(const std::string& name, const Pipeline::Configuration& config) {
	if (!p) {
		throw std::runtime_error("invalid pipeline factory");
	}
	p->pipelines[name] = config;
	return *this;
}


GraphicsPipelines::Factory& GraphicsPipelines::Factory::cache(VkPipelineCache cache) {
	if (!p) {
		throw std::runtime_error("invalid pipeline factory");
	}
	p->cache = cache;
	return *this;
}


std::vector<std::string> GraphicsPipelines::Factory::pipelines() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline factory");
	}
	std::vector<std::string> result;
	for (const auto& pipeline : p->pipelines) {
		result.push_back(pipeline.first);
	}
	return result;
}


const Pipeline::Configuration& GraphicsPipelines::Factory::operator[](const std::string& name) const {
	if (!p) {
		throw std::runtime_error("invalid pipeline factory");
	}
	return p->pipelines.at(name);
}


Pipeline::Configuration& GraphicsPipelines::Factory::operator[](const std::string& name) {
	if (!p) {
		throw std::runtime_error("invalid pipeline factory");
	}
	return p->pipelines.at(name);
}


VkPipelineCache GraphicsPipelines::Factory::cache() const {
	if (!p) {
		throw std::runtime_error("invalid pipeline factory");
	}
	return p->cache;
}


GraphicsPipelines GraphicsPipelines::Factory::create(VkDevice device, VkAllocationCallbacks* callbacks) const {
	return GraphicsPipelines(*this, device, callbacks);
}

}