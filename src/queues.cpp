#include <vkw/queues.hpp>

namespace vkw {

struct SubmitQueue::Pimpl {
	Pimpl(VkQueue q);
	~Pimpl() = default;

    VkQueue queue;
    VkFence fence;
    std::vector<VkSemaphore> wait, trigger;
    std::vector<VkPipelineStageFlags> stages;
    std::vector<VkCommandBuffer> buffers;
};


SubmitQueue::Pimpl::Pimpl(VkQueue q):
	queue{q},
	fence{VK_NULL_HANDLE}
{
}


SubmitQueue::SubmitQueue(VkQueue queue):
	p{nullptr}
{
	p = new Pimpl(queue);
}


SubmitQueue::~SubmitQueue() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


SubmitQueue& SubmitQueue::queue(VkQueue queue) {
	p->queue = queue;
	return *this;
}


SubmitQueue::SubmitQueue(SubmitQueue&& other):
	p{nullptr}
{
	p = other.p;
	other.p = nullptr;
}


SubmitQueue& SubmitQueue::operator=(SubmitQueue&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


VkQueue SubmitQueue::queue() const {
	return p->queue;
}


SubmitQueue& SubmitQueue::wait_for(VkSemaphore semaphore, VkPipelineStageFlags stage) {
	p->wait.push_back(semaphore);
	p->stages.push_back(stage);
	return *this;
}


SubmitQueue& SubmitQueue::will_unblock(VkFence fence) {
	p->fence = fence;
	return *this;
}


SubmitQueue& SubmitQueue::will_unblock(VkSemaphore semaphore) {
	p->trigger = { semaphore };
	return *this;
}


SubmitQueue& SubmitQueue::will_unblock(const std::vector<VkSemaphore> &semaphores) {
	p->trigger = semaphores;
	return *this;
}


SubmitQueue& SubmitQueue::command_buffer(VkCommandBuffer buffer) {
	p->buffers = std::vector<VkCommandBuffer>{ buffer };
	return *this;
}


SubmitQueue& SubmitQueue::command_buffers(const std::vector<VkCommandBuffer> &buffers) {
	p->buffers = buffers;
	return *this;
}


VkResult SubmitQueue::submit() const { 
	VkSubmitInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	info.waitSemaphoreCount = static_cast<uint32_t>(p->wait.size());
	info.pWaitSemaphores = !p->wait.empty() ? p->wait.data() : nullptr;
	info.pWaitDstStageMask = !p->stages.empty() ? p->stages.data() : nullptr;
	info.signalSemaphoreCount = static_cast<uint32_t>(p->trigger.size());
	info.pSignalSemaphores = !p->trigger.empty() ? p->trigger.data() : nullptr;
	info.commandBufferCount = static_cast<uint32_t>(p->buffers.size());
	info.pCommandBuffers = !p->buffers.empty() ? p->buffers.data() : nullptr;
	return vkQueueSubmit(p->queue, 1, &info, p->fence);
}


SubmitQueueGuard::SubmitQueueGuard(VkQueue queue):
	submit_queue_{queue}
{
}


SubmitQueueGuard::~SubmitQueueGuard() {
	if (submit_queue_.queue() != VK_NULL_HANDLE) {
		submit_queue_.submit();
	}
}


SubmitQueue& SubmitQueueGuard::submit_queue() {
	return submit_queue_;
}


QueueWaitIdleGuard::QueueWaitIdleGuard():queue_{VK_NULL_HANDLE} {

}


QueueWaitIdleGuard::~QueueWaitIdleGuard() {
	if (queue_ != VK_NULL_HANDLE) {
		vkQueueWaitIdle(queue_);
	}
}


QueueWaitIdleGuard& QueueWaitIdleGuard::queue(VkQueue q) {
	queue_ = q;
	return *this;
}


SubmitCommands::SubmitCommands(VkDevice device, uint32_t queue_family, uint32_t queue_idx):
		device_{device},
		queue_family_{queue_family} {
	vkGetDeviceQueue(device_, queue_family_, queue_idx, &queue_);
}


CommandBufferBegin SubmitCommands::begin() {
	pool_ = CommandPool(device_, queue_family_);
	alloc_ = pool_.allocate(1);
	auto result = alloc_.begin(0, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
	wait_start_.queue(queue_);
	submit_.submit_queue()
		.queue(queue_)
		.command_buffer(result.handle());
	wait_finish_.queue(queue_);
	return result;
}


struct PresentQueue::Pimpl {
    VkQueue queue;
    std::vector<VkSemaphore> wait;
    std::vector<VkSwapchainKHR> swapchain;
    std::vector<uint32_t> images;
};


PresentQueue::PresentQueue(VkQueue queue): 
	p{nullptr}
{
	p = new Pimpl();
	p->queue = queue;
}


PresentQueue::~PresentQueue() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


PresentQueue::PresentQueue(PresentQueue&& other):
	p{other.p}
{
	other.p = nullptr;
}


PresentQueue& PresentQueue::operator=(PresentQueue&& other) {
	if (p) {
		delete p;
	}
	p = other.p;
	other.p = nullptr;
	return *this;
}


PresentQueue& PresentQueue::waits(VkSemaphore semaphore) {
	p->wait = { semaphore };
	return *this;
}


PresentQueue& PresentQueue::waits(const std::vector<VkSemaphore> &semaphores) {
	p->wait = semaphores;
	return *this;
}


PresentQueue& PresentQueue::swapchain(VkSwapchainKHR chain) {
	p->swapchain = { chain };
	return *this;
}


PresentQueue& PresentQueue::swapchains(const std::vector<VkSwapchainKHR> &chains) {
	p->swapchain = chains;
	return *this;
}


PresentQueue& PresentQueue::image(uint32_t image) {
	p->images = { image };
	return *this;
}


PresentQueue& PresentQueue::images(const std::vector<uint32_t> &images) {
	p->images = images;
	return *this;
}


VkResult PresentQueue::present() const {
	VkPresentInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	info.waitSemaphoreCount = static_cast<uint32_t>(p->wait.size());
	info.pWaitSemaphores = !p->wait.empty() ? p->wait.data() : nullptr;
	info.swapchainCount = static_cast<uint32_t>(p->swapchain.size());
	info.pSwapchains = !p->swapchain.empty() ? p->swapchain.data() : nullptr;
	info.pImageIndices = !p->images.empty() ? p->images.data() : nullptr;
	return vkQueuePresentKHR(p->queue, &info);
}


Semaphore::Semaphore():handle_{VK_NULL_HANDLE} {
	
}


Semaphore::Semaphore(VkDevice device) : device_{device}, handle_{VK_NULL_HANDLE} {
	VkSemaphoreCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	if (vkCreateSemaphore(device_, &info, nullptr, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("failed to create semaphore");
	}
}


Semaphore::~Semaphore() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroySemaphore(device_, handle_, nullptr);
		handle_ = VK_NULL_HANDLE;
	}
}


Semaphore::Semaphore(Semaphore&& other):device_{other.device_}, handle_{other.handle_} {
	other.handle_ = VK_NULL_HANDLE;
}


Semaphore& Semaphore::operator=(Semaphore&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroySemaphore(device_, handle_, nullptr);
	}
	device_ = other.device_;
	handle_ = other.handle_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}


VkSemaphore Semaphore::handle() const { 
	return handle_; 
}


Fence::Fence():handle_{VK_NULL_HANDLE} {

}


Fence::Fence(VkDevice device, VkFenceCreateFlags flags) : device_{device}, handle_{VK_NULL_HANDLE} {
	VkFenceCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	info.flags = flags;
	if (vkCreateFence(device_, &info, nullptr, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("failed to create fence");
	}
}


Fence::~Fence() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyFence(device_, handle_, nullptr);
		handle_ = VK_NULL_HANDLE;
	}
}


Fence::Fence(Fence&& other):device_{other.device_}, handle_{other.handle_} {
	other.handle_ = VK_NULL_HANDLE;
}


Fence& Fence::operator=(Fence&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyFence(device_, handle_, nullptr);
	}
	device_ = other.device_;
	handle_ = other.handle_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}


VkFence Fence::handle() const { 
	return handle_; 
}


void Fence::wait(uint64_t timeout) const {
	vkWaitForFences(device_, 1, &handle_, VK_TRUE, timeout);
}


void Fence::reset() {
	vkResetFences(device_, 1, &handle_);
}

}