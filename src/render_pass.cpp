#include <vkw/render_pass.hpp>

namespace vkw {

RenderPass::Configuration::~Configuration() {

}


RenderPass::Commands::Commands(VkCommandBuffer buffer, const VkRenderPassBeginInfo* info, VkSubpassContents subpass_contents):
	buffer_{buffer} {
	vkCmdBeginRenderPass(buffer_, info, subpass_contents);
}

RenderPass::Commands::~Commands() {
	vkCmdEndRenderPass(buffer_);
}


RenderPass::RenderPass():device_{VK_NULL_HANDLE}, handle_{VK_NULL_HANDLE}, alloc_callbacks_{nullptr} {

}

RenderPass::RenderPass(const Configuration& cfg, VkDevice device, VkAllocationCallbacks* callbacks):
		device_{device}, handle_{VK_NULL_HANDLE}, alloc_callbacks_{callbacks} {

	auto attachments = cfg.attachments();
	auto subpasses = cfg.subpasses();
	auto subpass_dependencies = cfg.subpass_dependencies();

	VkRenderPassCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	info.attachmentCount = uint32_t(attachments.size());
	info.pAttachments = attachments.empty()? nullptr : attachments.data();
	info.subpassCount = uint32_t(subpasses.size());
	info.pSubpasses = subpasses.empty()? nullptr : subpasses.data();
	info.dependencyCount = uint32_t(subpass_dependencies.size());
	info.pDependencies = subpass_dependencies.empty()? nullptr : subpass_dependencies.data();

	if (vkCreateRenderPass(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create render pass");
	}
}

RenderPass::~RenderPass() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyRenderPass(device_, handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}

RenderPass::RenderPass(RenderPass&& other):device_{other.device_}, handle_{other.handle_}, alloc_callbacks_{other.alloc_callbacks_} {
	other.handle_ = VK_NULL_HANDLE;
}

RenderPass& RenderPass::operator=(RenderPass&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroyRenderPass(device_, handle_, alloc_callbacks_);
	}
	device_ = other.device_;
	handle_ = other.handle_;
	alloc_callbacks_ = other.alloc_callbacks_;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}

RenderPass::Commands RenderPass::begin(VkCommandBuffer buffer, VkFramebuffer framebuffer, VkRect2D area, const std::vector<VkClearValue>& clear_values) const {
	VkRenderPassBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	info.renderPass = handle_;
	info.framebuffer = framebuffer;
	info.renderArea = area;
	info.clearValueCount = uint32_t(clear_values.size());
	info.pClearValues = clear_values.empty()? nullptr : clear_values.data();
	return Commands(buffer, &info, VK_SUBPASS_CONTENTS_INLINE);
}

VkRenderPass RenderPass::handle() const {
	return handle_;
}

Attachment::Attachment(VkFormat format): info_{} {
	info_.format = format;
	info_.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	info_.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	info_.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	info_.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	info_.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	info_.finalLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	info_.samples = VK_SAMPLE_COUNT_1_BIT;
}

Attachment& Attachment::initial_layout(VkImageLayout layout) {
	info_.initialLayout = layout;
	return *this;
}

Attachment& Attachment::final_layout(VkImageLayout layout) {
	info_.finalLayout = layout;
	return *this;
}

VkAttachmentDescription Attachment::description() const {
	return info_;
}


struct Subpass::Pimpl {
	std::vector<VkAttachmentReference> input_attachments;
	std::vector<VkAttachmentReference> color_attachments;
	std::vector<uint32_t> preserve_attachments;
	VkAttachmentReference depth_stencil_attachment;
};


Subpass::Subpass(VkPipelineBindPoint bind_point):
	p{nullptr},
	bind_point_{bind_point} 
{
	p = new Pimpl();
}


Subpass::~Subpass() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


Subpass::Subpass(Subpass&& other)
{
	bind_point_ = other.bind_point_;
	p = other.p;
	other.p = nullptr;
}


Subpass& Subpass::operator=(Subpass&& other) {
	if (p) {
		delete p;
	}
	bind_point_ = other.bind_point_;
	p = other.p;
	other.p = nullptr;
	return *this;
}


Subpass& Subpass::input_attachments(const std::vector<VkAttachmentReference>& value) {
	p->input_attachments = value;
	return *this;
}


Subpass& Subpass::color_attachments(const std::vector<VkAttachmentReference>& value) {
	p->color_attachments = value;
	return *this;
}


Subpass& Subpass::preserve_attachments(const std::vector<uint32_t>& value) {
	p->preserve_attachments = value;
	return *this;
}


Subpass& Subpass::depth_stencil_attachment(VkAttachmentReference value) {
	p->depth_stencil_attachment = value;
	return *this;
}


VkSubpassDescription Subpass::description() const {
	VkSubpassDescription info = {};
	info.pipelineBindPoint = bind_point_;
	info.inputAttachmentCount = uint32_t(p->input_attachments.size());
	info.pInputAttachments = p->input_attachments.data();
	info.colorAttachmentCount = uint32_t(p->color_attachments.size());
	info.pColorAttachments = p->color_attachments.data();
	info.preserveAttachmentCount = uint32_t(p->preserve_attachments.size());
	info.pPreserveAttachments = p->preserve_attachments.data();
	info.pDepthStencilAttachment = &p->depth_stencil_attachment;
	return info;
}


SubpassDependency& SubpassDependency::dependency(VkDependencyFlags flags) {
	info_.dependencyFlags = flags;
	return *this;
}


SubpassDependency& SubpassDependency::subpasses(uint32_t src, uint32_t dst) {
	info_.srcSubpass = src;
	info_.dstSubpass = dst;
	return *this;
}


SubpassDependency& SubpassDependency::stage_masks(VkPipelineStageFlags src, VkPipelineStageFlags dst) {
	info_.srcStageMask = src;
	info_.dstStageMask = dst;
	return *this;
}


SubpassDependency& SubpassDependency::access_masks(VkAccessFlags src, VkAccessFlags dst) {
	info_.srcAccessMask = src;
	info_.dstAccessMask = dst;
	return *this;
}


SubpassDependency& SubpassDependency::source(uint32_t subpass, VkPipelineStageFlags stage_mask, VkAccessFlags access_mask) {
	info_.srcSubpass = subpass;
	info_.srcStageMask = stage_mask;
	info_.srcAccessMask = access_mask;
	return *this;
}


SubpassDependency& SubpassDependency::destination(uint32_t subpass, VkPipelineStageFlags stage_mask, VkAccessFlags access_mask) {
	info_.dstSubpass = subpass;
	info_.dstStageMask = stage_mask;
	info_.dstAccessMask = access_mask;
	return *this;
}


VkSubpassDependency SubpassDependency::description() const {
	return info_;
}


struct RenderPass::Factory::Pimpl {
	std::vector<VkAttachmentDescription> attachments;
	std::vector<VkSubpassDescription> subpasses;
	std::vector<VkSubpassDependency> dependencies;
};


RenderPass::Factory::Factory():
	p{nullptr}
{
	p = new Pimpl();
}


RenderPass::Factory::~Factory() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


std::vector<VkAttachmentDescription> RenderPass::Factory::attachments() const {
	return p->attachments;
}


std::vector<VkSubpassDescription> RenderPass::Factory::subpasses() const {
	return p->subpasses;
}


std::vector<VkSubpassDependency> RenderPass::Factory::subpass_dependencies() const {
	return p->dependencies;
}


RenderPass::Factory& RenderPass::Factory::attachments(const std::vector<Attachment>& values) {
	p->attachments.resize(values.size());
	auto get_description = [](const Attachment& value) -> VkAttachmentDescription { 
		return value.description(); 
	};
	std::transform(values.begin(), values.end(), p->attachments.begin(), get_description);
	return *this;
}


RenderPass::Factory& RenderPass::Factory::subpasses(const std::vector<Subpass>& values) {
	p->subpasses.resize(values.size());
	auto get_description = [](const Subpass& value) -> VkSubpassDescription { 
		return value.description(); 
	};
	std::transform(values.begin(), values.end(), p->subpasses.begin(), get_description);
	return *this;
}


RenderPass::Factory& RenderPass::Factory::subpass_dependencies(const std::vector<SubpassDependency>& values) {
	p->dependencies.resize(values.size());
	auto get_description = [](const SubpassDependency& value) -> VkSubpassDependency { 
		return value.description(); 
	};
	std::transform(values.begin(), values.end(), p->dependencies.begin(), get_description);
	return *this;
}


RenderPass RenderPass::Factory::create(VkDevice device, VkAllocationCallbacks* callbacks) {
	return RenderPass(*this, device, callbacks);
}

}