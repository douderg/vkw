#include <vkw/sampler.hpp>

namespace vkw {

Sampler::Sampler():handle_{VK_NULL_HANDLE} {

}


Sampler::Sampler(VkDevice device, VkSampler sampler, VkAllocationCallbacks* alloc_callbacks):
    device_{device},
    handle_{sampler},
    alloc_callbacks_{alloc_callbacks} 
{
    
}


Sampler::~Sampler() {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroySampler(device_, handle_, alloc_callbacks_);
        handle_ = VK_NULL_HANDLE;
    }
}


Sampler::Sampler(Sampler&& other):
        device_{other.device_},
        handle_{other.handle_},
        alloc_callbacks_{other.alloc_callbacks_} {
    other.handle_ = VK_NULL_HANDLE;
}


Sampler& Sampler::operator=(Sampler&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroySampler(device_, handle_, alloc_callbacks_);
    }
    device_ = other.device_;
    handle_ = other.handle_;
    alloc_callbacks_ = other.alloc_callbacks_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}


VkSampler Sampler::handle() const {
    return handle_;
}


Sampler::Factory::Factory():
    info_{}
{
    info_.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    info_.magFilter = VK_FILTER_NEAREST;
    info_.minFilter = VK_FILTER_NEAREST;
    info_.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    info_.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    info_.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    info_.anisotropyEnable = VK_FALSE;
    info_.maxAnisotropy = 1.f;
    info_.borderColor = VK_BORDER_COLOR_INT_TRANSPARENT_BLACK;
    info_.unnormalizedCoordinates = VK_FALSE;
    info_.compareEnable = VK_FALSE;
    info_.compareOp = VK_COMPARE_OP_ALWAYS;
    info_.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    info_.mipLodBias = 0.f;
    info_.minLod = 0.f;
    info_.maxLod = 0.f;
}


Sampler Sampler::Factory::create(VkDevice device, VkAllocationCallbacks* callbacks) const {
    VkSampler sampler = VK_NULL_HANDLE;
    if (vkCreateSampler(device, &info_, callbacks, &sampler) != VK_SUCCESS) {
        throw std::runtime_error("failed to create sampler");
    }
    return Sampler(device, sampler, callbacks);
}

}