#include <vkw/shader.hpp>
#include <sstream>
#include <fstream>

namespace vkw {

ShaderModule::ShaderModule():
        handle_{VK_NULL_HANDLE},
        device_{VK_NULL_HANDLE},
        alloc_callbacks_{nullptr} {

}

ShaderModule::ShaderModule(VkDevice device, const std::filesystem::path& filename, VkAllocationCallbacks* callbacks):
        handle_{VK_NULL_HANDLE},
        device_{device},
        alloc_callbacks_{callbacks} {

    VkShaderModuleCreateInfo info = {};
    std::ifstream f(filename, std::ios::ate | std::ios::binary);
    if (!f) {
        throw std::runtime_error("Error: failed to open file '" + filename.string() + "'");
    }
    
    auto len = f.tellg();
    std::vector<char> code;
    code.resize(len);
    f.seekg(0, f.beg);
    f.read(code.data(), len);
    info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    info.codeSize = len;
    info.pCode = reinterpret_cast<const uint32_t*>(code.data());

    if (vkCreateShaderModule(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("Error: failed to build shader module");
    }
}

ShaderModule::ShaderModule(VkDevice device, uint32_t* data, size_t bytes, VkAllocationCallbacks* callbacks): 
        handle_{VK_NULL_HANDLE},
        device_{device},
        alloc_callbacks_{callbacks} {
    VkShaderModuleCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    info.codeSize = bytes;
    info.pCode = data;
    if (vkCreateShaderModule(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("Error: failed to build shader module");
    }
}

ShaderModule::ShaderModule(ShaderModule&& other):
        handle_{std::move(other.handle_)},
        device_{std::move(other.device_)},
        alloc_callbacks_{std::move(other.alloc_callbacks_)} {
    other.handle_ = VK_NULL_HANDLE;
}

ShaderModule::~ShaderModule() {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyShaderModule(device_, handle_, alloc_callbacks_);
        handle_ = VK_NULL_HANDLE;
    }
}

ShaderModule& ShaderModule::operator=(ShaderModule&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        vkDestroyShaderModule(device_, handle_, alloc_callbacks_);
    }
    handle_ = std::move(other.handle_);
    device_ = std::move(other.device_);
    alloc_callbacks_ = std::move(other.alloc_callbacks_);
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

VkShaderModule ShaderModule::handle() const {
    return handle_;
}

}