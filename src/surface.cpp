#include <vkw/surface.hpp>
#ifdef VKW_USE_SDL
#include <SDL_vulkan.h>
#endif
#include <stdexcept>
#include <algorithm>

namespace vkw {

Surface::Surface():handle_{VK_NULL_HANDLE} {

}

#ifdef VK_USE_PLATFORM_XCB_KHR

Surface::Surface(VkInstance instance, xcb_connection_t* connection, xcb_window_t window, VkAllocationCallbacks* callbacks):
	handle_{VK_NULL_HANDLE}, 
	instance_{instance}, 
	alloc_callbacks_{callbacks} 
{
	VkXcbSurfaceCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	info.pNext = nullptr;
	info.connection = connection;
	info.window = window;
	if (vkCreateXcbSurfaceKHR(instance_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("failed to create xcb surface");
	}
}

std::vector<const char*> Surface::required_extensions() {
	return std::vector<const char*>({
		VK_KHR_SURFACE_EXTENSION_NAME, 
		VK_KHR_XCB_SURFACE_EXTENSION_NAME });
}
#endif

#ifdef VK_USE_PLATFORM_WIN32_KHR

Surface::Surface(VkInstance instance, HINSTANCE proc, HWND window, VkAllocationCallbacks* callbacks):
	handle_{VK_NULL_HANDLE}, 
	instance_{instance}, 
	alloc_callbacks_{callbacks} 
{
	VkWin32SurfaceCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	info.hinstance = proc;
	info.hwnd = window;
	if (vkCreateWin32SurfaceKHR(instance_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("failed to create win32 surface");
	}
}

std::vector<const char*> Surface::required_extensions() {
	return std::vector<const char*>({
		VK_KHR_SURFACE_EXTENSION_NAME, 
		VK_KHR_WIN32_SURFACE_EXTENSION_NAME });
}
#endif

#ifdef VK_USE_PLATFORM_XLIB_KHR
Surface::Surface(VkInstance instance, Display* display, Window window, VkAllocationCallbacks* callbacks):
	handle_{nullptr}, 
	instance_{instance},
	alloc_callbacks_{callbacks}
{
	VkXlibSurfaceCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
	info.pNext = nullptr;
	info.dpy = display;
	info.window = window;
	if (vkCreateXlibSurfaceKHR(instance_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		throw std::runtime_error("failed to create xlib surface");
	}
}

std::vector<const char*> Surface::required_extensions() {
	return std::vector<const char*>({
		VK_KHR_SURFACE_EXTENSION_NAME, 
		VK_KHR_XLIB_SURFACE_EXTENSION_NAME });
}
#endif

#ifdef VKW_USE_SDL
Surface::Surface(VkInstance instance, SDL_Window* window, VkAllocationCallbacks* callbacks):
	handle_{VK_NULL_HANDLE}, 
	instance_{instance}, 
	alloc_callbacks_{callbacks}  
{
	if (SDL_Vulkan_CreateSurface(window, instance_, &handle_) != SDL_TRUE) {
		throw std::runtime_error("error: failed to create surface");
	}
}
#endif

Surface::~Surface() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroySurfaceKHR(instance_, handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
}

Surface::Surface(Surface&& other):
	instance_{other.instance_},
	handle_{other.handle_},
	alloc_callbacks_{other.alloc_callbacks_}
{
	other.instance_ = VK_NULL_HANDLE;
	other.handle_ = VK_NULL_HANDLE;
}

Surface& Surface::operator=(Surface&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroySurfaceKHR(instance_, handle_, alloc_callbacks_);
	}
	instance_ = other.instance_;
	handle_ = other.handle_;
	alloc_callbacks_ = other.alloc_callbacks_;
	other.instance_ = VK_NULL_HANDLE;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}

std::vector<VkSurfaceFormatKHR> Surface::supported_formats(VkPhysicalDevice device) const {
	uint32_t count;
	std::vector<VkSurfaceFormatKHR> result;
	if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, handle_, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("failed to get surface supported formats");
	}
	result.resize(count);
	if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, handle_, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to get surface supported formats");
	}
	return result;
}

std::vector<VkPresentModeKHR> Surface::present_modes(VkPhysicalDevice device) const {
	uint32_t count;
	std::vector<VkPresentModeKHR> result;
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, handle_, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("failed to get surface present modes");
	}
	result.resize(count);
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, handle_, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to get surface present modes");
	}
	return result;
}

VkSurfaceCapabilitiesKHR Surface::capabilities(VkPhysicalDevice device) const {
	VkSurfaceCapabilitiesKHR result;
	if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, handle_, &result) != VK_SUCCESS) {
		throw std::runtime_error("failed to get surface capabilities");
	}
	return result;
}

VkSurfaceKHR Surface::handle() const {
	return handle_;
}

VkSurfaceFormatKHR Surface::select_format(VkPhysicalDevice device, const std::vector<VkSurfaceFormatKHR>& preffered_formats) const {
	auto supported = supported_formats(device);
	for (auto format : preffered_formats) {
		auto matches_next = [preffered=format](VkSurfaceFormatKHR supported) -> bool {
			return supported.format == VK_FORMAT_UNDEFINED || supported.format == preffered.format && supported.colorSpace == preffered.colorSpace;
		};
		if (std::any_of(supported.begin(), supported.end(), matches_next)) {
			return format;
		}
	}
	throw std::runtime_error("error: could not pick appropriate image format");
}

}

