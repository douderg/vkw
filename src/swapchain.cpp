#include <vkw/swapchain.hpp>

namespace vkw {


struct Swapchain::Pimpl {
	std::vector<ImageView> images;
	std::vector<Framebuffer> framebuffers;
};


Swapchain::Swapchain(VkDevice device, VkAllocationCallbacks* callbacks):
	p{nullptr},
	device_{device}, 
	handle_{VK_NULL_HANDLE}, 
	alloc_callbacks_{callbacks} 
{
	p = new Pimpl();
}


Swapchain::Swapchain(VkDevice device, const Configuration& cfg, VkRenderPass render_pass, VkAllocationCallbacks* callbacks):
	Swapchain(device, callbacks)
{
	create(cfg, render_pass);
}


Swapchain::Swapchain(VkDevice device, const Configuration& cfg, VkRenderPass render_pass, VkImageView depth_buffer, VkAllocationCallbacks* callbacks):
	Swapchain(device, callbacks)
{
	create(cfg, render_pass, depth_buffer);
}


Swapchain::~Swapchain() {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroySwapchainKHR(device_, handle_, alloc_callbacks_);
		handle_ = VK_NULL_HANDLE;
	}
	if (p) {
		delete p;
		p = nullptr;
	}
}


Swapchain::Swapchain(Swapchain&& other):
	device_{other.device_}, 
	handle_{other.handle_}, 
	alloc_callbacks_{other.alloc_callbacks_} 
{
	other.handle_ = VK_NULL_HANDLE;
	p = other.p;
	other.p = nullptr;
}


Swapchain& Swapchain::operator=(Swapchain&& other) {
	if (handle_ != VK_NULL_HANDLE) {
		vkDestroySwapchainKHR(device_, handle_, alloc_callbacks_);
	}
	if (p) {
		delete p;
	}
	device_ = other.device_;
	handle_ = other.handle_;
	alloc_callbacks_ = other.alloc_callbacks_;
	p = other.p;
	other.p = nullptr;
	other.handle_ = VK_NULL_HANDLE;
	return *this;
}


std::vector<VkImage> Swapchain::get_images() const {
	uint32_t count;
	if (vkGetSwapchainImagesKHR(device_, handle_, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get swapchain images");
	}
	std::vector<VkImage> result;
	if (!count) {
		return result;
	}
	result.resize(count);
	if (vkGetSwapchainImagesKHR(device_, handle_, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get swapchain images");
	}
	return result;
}


VkFramebuffer Swapchain::framebuffer(size_t index) const {
	return p->framebuffers.at(index).handle();
}


Swapchain& Swapchain::create(const Configuration& cfg, VkRenderPass render_pass) {
	p->framebuffers.clear();
	p->images.clear();
	VkSwapchainKHR old = handle_;
	VkSwapchainCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	info.surface = cfg.surface();
	info.minImageCount = cfg.surface_capabilities().minImageCount;
	info.imageFormat = cfg.image_format().format;
	info.imageColorSpace = cfg.image_format().colorSpace;
	info.imageExtent = cfg.extent();
	info.imageArrayLayers = cfg.image_layers();
	info.imageUsage = cfg.image_usage();
	info.imageSharingMode = cfg.sharing_mode();
	info.queueFamilyIndexCount = cfg.queue_indices().size();
	info.pQueueFamilyIndices = cfg.queue_indices().data();
	info.preTransform = cfg.surface_capabilities().currentTransform;
	info.compositeAlpha = cfg.composite_alpha();
	info.presentMode = cfg.present_mode();
	info.clipped = cfg.is_clipped();
	info.oldSwapchain = handle_;
	if (vkCreateSwapchainKHR(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		handle_ = VK_NULL_HANDLE;
		throw std::runtime_error("failed to create swapchain");
	}
	if (old != VK_NULL_HANDLE) {
		vkDestroySwapchainKHR(device_, old, alloc_callbacks_);
	}

	auto images = get_images();
	for (auto image : images) {
		p->images.emplace_back(device_, image, info.imageFormat);
	}

	for (const auto& image : p->images) {
		std::vector<VkImageView> views {image.handle()};
		p->framebuffers.emplace_back(device_, views, cfg.extent(), render_pass, 1);
	}

	return *this;
}


Swapchain& Swapchain::create(const Configuration& cfg, VkRenderPass render_pass, VkImageView depth_view) {
	p->framebuffers.clear();
	p->images.clear();
	VkSwapchainKHR old = handle_;
	VkSwapchainCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	info.surface = cfg.surface();
	info.minImageCount = cfg.surface_capabilities().minImageCount;
	info.imageFormat = cfg.image_format().format;
	info.imageColorSpace = cfg.image_format().colorSpace;
	info.imageExtent = cfg.extent();
	info.imageArrayLayers = cfg.image_layers();
	info.imageUsage = cfg.image_usage();
	info.imageSharingMode = cfg.sharing_mode();
	info.queueFamilyIndexCount = cfg.queue_indices().size();
	info.pQueueFamilyIndices = cfg.queue_indices().data();
	info.preTransform = cfg.surface_capabilities().currentTransform;
	info.compositeAlpha = cfg.composite_alpha();
	info.presentMode = cfg.present_mode();
	info.clipped = cfg.is_clipped();
	info.oldSwapchain = handle_;
	if (vkCreateSwapchainKHR(device_, &info, alloc_callbacks_, &handle_) != VK_SUCCESS) {
		handle_ = VK_NULL_HANDLE;
		throw std::runtime_error("failed to create swapchain");
	}
	if (old != VK_NULL_HANDLE) {
		vkDestroySwapchainKHR(device_, old, alloc_callbacks_);
	}

	auto images = get_images();
	for (auto image : images) {
		p->images.emplace_back(device_, image, info.imageFormat);
	}

	for (const auto& image : p->images) {
		std::vector<VkImageView> views {image.handle(), depth_view};
		p->framebuffers.emplace_back(device_, views, cfg.extent(), render_pass, cfg.image_layers());
	}

	return *this;
}


VkSwapchainKHR Swapchain::handle() const {
	return handle_;
}


size_t Swapchain::image_count() const {
	return p->images.size();
}


VkResult Swapchain::acquire_next_image(uint32_t* result, VkSemaphore lock, size_t timeout) const {
	return vkAcquireNextImageKHR(
		device_,
		handle_,
		timeout,
		lock,
		VK_NULL_HANDLE,
		result);
}


struct Swapchain::Factory::Pimpl {
	std::vector<uint32_t> queue_indices;
};


Swapchain::Factory::Factory():
	p{nullptr},
	surface_{VK_NULL_HANDLE},
	capabilities_{},
	format_{VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR},
	image_usage_{VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT},
	layers_{1},
	sharing_mode_{VK_SHARING_MODE_EXCLUSIVE},
	extent_{},
	present_mode_{VK_PRESENT_MODE_FIFO_KHR},
	composite_alpha_{VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR},
	clipped_{VK_TRUE}
{
	p = new Pimpl();
}


Swapchain::Factory::~Factory() {
	if (p) {
		delete p;
		p = nullptr;
	}
}


VkSurfaceKHR Swapchain::Factory::surface() const {
	return surface_;
}


VkSurfaceCapabilitiesKHR Swapchain::Factory::surface_capabilities() const {
	return capabilities_;
}


VkSurfaceFormatKHR Swapchain::Factory::image_format() const {
	return format_;
}


VkImageUsageFlagBits Swapchain::Factory::image_usage() const {
	return image_usage_;
}


uint32_t Swapchain::Factory::image_layers() const {
	return layers_;
}


VkSharingMode Swapchain::Factory::sharing_mode() const {
	return sharing_mode_;
}


VkExtent2D Swapchain::Factory::extent() const {
	return extent_;
}


VkPresentModeKHR Swapchain::Factory::present_mode() const {
	return present_mode_;
}


VkCompositeAlphaFlagBitsKHR Swapchain::Factory::composite_alpha() const {
	return composite_alpha_;
}


VkBool32 Swapchain::Factory::is_clipped() const {
	return clipped_;
}


const std::vector<uint32_t>& Swapchain::Factory::queue_indices() const {
	return p->queue_indices;
}


Swapchain::Factory& Swapchain::Factory::surface(VkSurfaceKHR value) {
	surface_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::surface_capabilities(VkSurfaceCapabilitiesKHR value) {
	capabilities_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::image_format(VkSurfaceFormatKHR value) {
	format_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::image_usage(VkImageUsageFlagBits value) {
	image_usage_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::image_layers(uint32_t value) {
	layers_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::sharing_mode(VkSharingMode value) {
	sharing_mode_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::extent(const VkExtent2D& value) {
	extent_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::present_mode(VkPresentModeKHR value) {
	present_mode_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::composite_alpha(VkCompositeAlphaFlagBitsKHR value) {
	composite_alpha_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::is_clipped(VkBool32 value) {
	clipped_ = value;
	return *this;
}


Swapchain::Factory& Swapchain::Factory::queue_indices(const std::vector<uint32_t>& indices) {
	p->queue_indices = indices;
	return *this;
}


Swapchain Swapchain::Factory::create(VkDevice device, VkRenderPass render_pass) {
	return Swapchain(device, *this, render_pass);
}


Swapchain Swapchain::Factory::create(VkDevice device, VkRenderPass render_pass, VkImageView depth_buffer) {
	return Swapchain(device, *this, render_pass, depth_buffer);
}

}