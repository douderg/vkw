#include <vkw/validation.hpp>
#include <iostream>

namespace vkw {

namespace validation {

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData) {
    std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
    return VK_FALSE;
}

VkResult createDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void destroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT dbg, 
        const VkAllocationCallbacks* pAllocator) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
        func(instance, dbg, pAllocator);
    }
}

}

DebugMessenger::DebugMessenger():
    instance_{VK_NULL_HANDLE},
    handle_{VK_NULL_HANDLE}
{
}

DebugMessenger::DebugMessenger(VkInstance instance): instance_{instance}, handle_{VK_NULL_HANDLE} {
    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = validation::debugCallback;
    createInfo.pUserData = nullptr; // Optional

    if (validation::createDebugUtilsMessengerEXT(instance_, &createInfo, nullptr, &handle_) != VK_SUCCESS) {
        throw std::runtime_error("failed to create debug messenger");
    }
}

DebugMessenger::~DebugMessenger() {
    if (handle_ != VK_NULL_HANDLE) {
        validation::destroyDebugUtilsMessengerEXT(instance_, handle_, nullptr);
        handle_ = VK_NULL_HANDLE;
    }
}

DebugMessenger::DebugMessenger(DebugMessenger&& other):
    instance_{other.instance_},
    handle_{other.handle_}
{
    other.handle_ = VK_NULL_HANDLE;
}

DebugMessenger& DebugMessenger::operator=(DebugMessenger&& other) {
    if (handle_ != VK_NULL_HANDLE) {
        validation::destroyDebugUtilsMessengerEXT(instance_, handle_, nullptr);
    }
    instance_ = other.instance_;
    handle_ = other.handle_;
    other.handle_ = VK_NULL_HANDLE;
    return *this;
}

}