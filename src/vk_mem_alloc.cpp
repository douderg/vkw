#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
#include <vkw/allocator.hpp>

namespace vkw {
VkDevice get_allocator_device(VmaAllocator allocator) {
    return allocator->m_hDevice;
}
}