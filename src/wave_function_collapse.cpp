#include <vkw/wave_function_collapse.hpp>
#include <cassert>

namespace vkw {

struct entropy_order {
    entropy_order(const std::vector<size_t>& ent):
        entropy{ent}
    {
    }

    bool operator()(uint64_t x, uint64_t y) const {
        return entropy[x] > entropy[y];
    }

    const std::vector<size_t>& entropy;
};


WaveFunctionCollapse::WaveFunctionCollapse(size_t node_count):
    links_(node_count),
    available_(node_count),
    fixed_nodes_(node_count),
    selected_states_(node_count)
{
}


bool WaveFunctionCollapse::select_state_mut(uint64_t node, uint64_t selected_state, step_data& data) {

    if (!select_state(node, selected_state, data)) {
        return false;
    }
    bool cleanup = false;

    for (const auto& d : data.disabled_in_step) {
        for (uint64_t state : d.second) {
            available_[d.first].erase(state);
        }
        if (available_[d.first].empty()) {
            cleanup = true;
        }
    }

    if (cleanup) {
        for (const auto& d : data.disabled_in_step) {
            if (d.first != node) {
                available_[d.first].insert(d.second.begin(), d.second.end());
            }
        }
        data.disabled_in_step.clear();
        return false;
    }
    return true;
}


bool WaveFunctionCollapse::select_state(uint64_t node, uint64_t selected_state, step_data& data) {
    data.selected_state = selected_state;

    for (const auto& l : links_[node]) {
        uint64_t neighbor = l.first;

        if (fixed_nodes_[neighbor]) {
            auto it = links_[neighbor].find(node);
            if (it != links_[neighbor].end()) {
                for (auto profile_index : it->second) {
                    auto it2 = profiles_[profile_index].find(selected_states_[neighbor]);
                    if (it2 != profiles_[profile_index].end()) {
                        auto it3 = it2->second.find(selected_state);
                        if (it3 != it2->second.end()) {
                            return false;
                        }
                    }
                }
            }

            for (auto profile_index : l.second) {
                auto it2 = profiles_[profile_index].find(selected_state);
                if (it2 != profiles_[profile_index].end()) {
                    auto it3 = it2->second.find(selected_states_[neighbor]);
                    if (it3 != it2->second.end()) {
                        return false;
                    }
                }
            }
        }

        if (!fixed_nodes_[neighbor] && available_[neighbor].empty()) {
            return false;
        }

        if (l.second.empty()) {
            continue;
        }

        std::vector<std::set<uint64_t>::const_iterator> its;
        std::vector<std::set<uint64_t>::const_iterator> ends;
        std::vector<size_t> indices;

        auto it = available_[neighbor].begin();
        for (size_t profile : l.second) {
            auto iter = profiles_[profile].find(selected_state);
            if (iter != profiles_[profile].end()) {
                auto begin = iter->second.lower_bound(*it);
                if (begin == iter->second.end()) {
                    continue;
                }
                its.push_back(begin);
                ends.push_back(iter->second.end());
                indices.emplace_back();
            }
        }

        if (indices.empty()) {
            continue;
        }

        std::iota(indices.begin(), indices.end(), 0);

        auto comp = [&its, &ends](size_t x, size_t y)->bool {
            if (its[x] == ends[x]) {
                return true;
            } else if (its[y] == ends[y]) {
                return false;
            } else {
                return *its[x] > *its[y];
            }
        };

        std::make_heap(indices.begin(), indices.end(), comp);
        std::pop_heap(indices.begin(), indices.end(), comp);
        
        
        while (it != available_[neighbor].end()) {
            size_t min = indices.back();
            if (its[min] == ends[min]) {
                break;
            }
            if (*it < *its[min]) {
                ++it;
            } else if (*its[min] < *it) {
                ++its[min];
                std::make_heap(indices.begin(), indices.end(), comp);
                std::pop_heap(indices.begin(), indices.end(), comp);
            } else {
                data.disabled_in_step[neighbor].insert(*it);
                ++it;
            }
        }
    }

    data.selected_node = node;
    return true;
}

void WaveFunctionCollapse::add_profile(const std::map<uint64_t, std::set<uint64_t>>& states) {
    profiles_.emplace_back(states);
}

std::vector<uint64_t> WaveFunctionCollapse::evaluate(unsigned int seed) {
    std::stack<step_data> steps;

    auto& init = steps.emplace();

    entropies_.resize(links_.size());
    for (size_t i = 0; i < available_.size(); ++i) {
        entropies_[i] = available_[i].size();
    }

    entropy_order order(entropies_);
    std::vector<size_t> node_order(entropies_.size());
    std::iota(node_order.begin(), node_order.end(), 0);
    std::make_heap(node_order.begin(), node_order.end(), order);
    std::pop_heap(node_order.begin(), node_order.end(), order);
    init.selected_node = node_order.back();

    size_t min_depth = entropies_.size();
    std::default_random_engine rng(seed);
    std::uniform_real_distribution<double> dist(0.0, 1.0);
    while (!steps.empty()) {
        uint64_t selected_node = steps.top().selected_node;
        
        if (!available_[selected_node].empty()) {
            auto state = available_[selected_node].begin();
            size_t x = std::floor(dist(rng) * available_[selected_node].size());
            for (size_t i = 0; i < x; ++i) {
                ++state;
            }
            uint64_t selected_state = *state;
            
            step_data next_step;
            if (select_state_mut(steps.top().selected_node, selected_state, next_step)) {
                available_[selected_node].erase(selected_state);
                steps.top().disabled_in_step[selected_node].insert(selected_state);
                next_step.disabled_in_step[selected_node].erase(selected_state);

                selected_states_[selected_node] = selected_state;
                if (steps.size() == fixed_nodes_.size()) {
                    return selected_states_;
                }
                fixed_nodes_[selected_node] = true;
                
                for (size_t i = 0; i < available_.size(); ++i) {
                    entropies_[i] = available_[i].size();
                }

                node_order.pop_back();
                std::make_heap(node_order.begin(), node_order.end(), order);
                std::pop_heap(node_order.begin(), node_order.end(), order);
                next_step.selected_node = node_order.back();
                steps.push(next_step);
                assert(next_step.selected_node != selected_node);

            } else {
                available_[selected_node].erase(selected_state);
                steps.top().disabled_in_step[selected_node].insert(selected_state);
            }
        } else {
            uint64_t drained_node = steps.top().selected_node;
            std::set<uint64_t> disabled_states;
            for (const auto& disabled :  steps.top().disabled_in_step) {
                available_[disabled.first].insert(disabled.second.begin(), disabled.second.end());
            }
            
            steps.pop();
            if (fixed_nodes_[drained_node]) {
                node_order.push_back(drained_node);
            } else if (!steps.empty()) {
                node_order.push_back(steps.top().selected_node);
                fixed_nodes_[steps.top().selected_node] = false;
            }
            fixed_nodes_[drained_node] = false;
            if (!steps.empty()) {
                available_[steps.top().selected_node].erase(steps.top().selected_state);
                steps.top().disabled_in_step[steps.top().selected_node].insert(steps.top().selected_state);
            }
        }
    }

    return std::vector<uint64_t>();
}

void WaveFunctionCollapse::set_available_states(uint64_t node, const std::set<uint64_t>& states) {
    available_[node] = states;
}

void WaveFunctionCollapse::link_nodes(uint64_t src, uint64_t dst, uint64_t profile_index) {
    links_[src][dst].emplace_back(profile_index);
}

}